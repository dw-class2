<?php
/**
 * Classification structure administration
 */
use Tools\Debug,
	Tools\DwHelper,
	Tools\Globals,
	Entities\Column,
	Facade\Courses,
	Facade\Parts,
	Facade\Columns,
	UI\Table;

require_once(dirname(__FILE__).'/../bootstrap.php');

if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

class admin_plugin_class2_main extends DokuWiki_Admin_Plugin {

	private $course = NULL;
	private $part = NULL;
	private $mode = NULL;
	private $courses = NULL;
	private $parts = NULL;
	private $columns = NULL;

	public function __construct() {
		//Debug::setDebugTarget(Debug::DEBUG_OFF);
		Debug::setDebugTarget(Debug::DEBUG_DW_MSG);
	}

/**
 * overridden translation method
 * @return translation or code
 */
	public function getLang($code) {
		$translation = trim(parent::getLang($code));
		if(empty($translation))
			return $code;
		return $translation;
	}
/**
 * Returns basic information about the plug-in required by Dokuwiki
 * @return array with basic information
 */
	function getInfo() {
		return array(
			'author'=> 'vojta jirkovsky',
			'email' => 'jirkovoj@fit.cvut.cz',
			'date' => '2010-2011',
			'name' => 'classification 2 - admin',
			'desc' => 'classification 2 score admin plugin (classification structure administration)',
			'url' => 'http://edux.fit.cvut.cz/',
		);
	}
	
	public function getMenuText() {
		//spravne se resi pomoci $lang['menu']
		return 'Classification structure administration';
	}

	public function forAdminsOnly() {
		return false;
	}

	private function saveColumns() {
		$this->course = $_REQUEST['columns']['course'];
		$this->part = $_REQUEST['columns']['part'];
		foreach($_REQUEST['column'] as $id => $cdata) {
			foreach($cdata as $ckey => $cvalue) {
				if(!$cvalue) {
					$cdata[$ckey] = NULL;
				}
			}
			$em = Globals::getEntityManager();
			if($cdata['variable']) {
				if(!($column = Columns::getColumnById($id))) {
					$column = new Column;
					$part = Parts::getPartByCourseAndName($this->course,$this->part);
					$column->part($part);
				}
				try {
					$column->idx($cdata['idx'] ? $cdata['idx'] : 0);
					$column->variable($cdata['variable']);
					$column->caption($cdata['caption']);
					$column->type($cdata['type']);
					$column->expression($cdata['expression']);
					$column->defaultValue($cdata['defaultValue']);
					$column->validRange($cdata['validRange']);
					$column->validRegex($cdata['validRegex']);
					$column->validValues($cdata['validValues']);
					$em->persist($column);
				} catch(Exception $e) {
					Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
				}
			}
		}
		$em->flush();
	}

	public function handle() {
		//manage form save
		if(isset($_REQUEST['columns'])) {
			$this->saveColumns();
		}
		//manage session
		if(isset($_REQUEST['course'])) {
			DwHelper::setSession('admin.course',$_REQUEST['course']);
		}
		if(isset($_REQUEST['part'])) {
			DwHelper::setSession('admin.part',$_REQUEST['part']);
		}
		
		$this->course = DwHelper::getSession('admin.course');
		$this->part = DwHelper::getSession('admin.part');

		//page mode
		if($this->part) {
			$this->mode = 'columns';
			$this->columns = Columns::getColumnsByCourseAndPart($this->course,$this->part);
		} else if($this->course) {
			$this->mode = 'parts';
			$this->parts = Parts::getPartsByCourse($this->course);
		} else {
			$this->mode = 'courses';
			$this->courses = Courses::getAllCourses();
		}

	}

	public function html() {
		global $ID;

		echo '<div class="adminHeader">';
		echo '<a href="?id='.$ID.'&do='.'admin'.'&page='.'class2_main'.'&course=&part=">Courses</a>';
		if($this->course) {
			echo ' ('.$this->course.') / <a href="?id='.$ID.'&do='.'admin'.'&page='.'class2_main'.'&part=">Parts</a>';
			if($this->part) {
				echo ' ('.$this->part.')';
			}
		}
		echo ' | ';
		echo 'Edit '.$this->mode;
		echo '</div>';
		switch($this->mode) {
			case 'saveColumns':
				break;
			case 'columns':
				$columnTypes = array(
					'number' => 'number',
					'boolean' => 'boolean',
					'string' => 'string',
					'expression' => 'expression',
					'enum' => 'enum',
				);
				$table = new Table;
				$table->editable(TRUE);
				$table->form->csrf(DwHelper::getCSRF());
				$table->form->addHidden('id',$ID);
				$table->form->addHidden('do','admin');
				$table->form->addHidden('page','class2_main');
				$table->form->addHidden('columns[course]',$this->course);
				$table->form->addHidden('columns[part]',$this->part);
				$table->form->action("?id=$ID&do=admin&page=class2_main");
				$table->form->setSubmit('Save');
				$table->addHeadRow()
						->add('idx')->add('variable')->add('caption')->add('type')
						->add('expression')->add('default value')->add('valid range')
						->add('valid regex')->add('valid values');
				foreach($this->columns as $column) {
					$id = $column->id();
					$table->addBodyRow()
						->addInput($column->idx(),"column[$id][idx]")
						->addInput($column->variable(),"column[$id][variable]")
						->addInput($column->caption(),"column[$id][caption]")
						->addSelect(array('selected'=>$column->type(),'data'=>$columnTypes),"column[$id][type]")
						->addInput($column->expression(),"column[$id][expression]")
						->addInput($column->defaultValue(),"column[$id][defaultValue]")
						->addInput($column->validRange(),"column[$id][validRange]")
						->addInput($column->validRegex(),"column[$id][validRegex]")
						->addInput(implode(',',$column->validValues()),"column[$id][validValues]");
				}
				$table->addBodyRow()
					->addInput(NULL,"column[new0][idx]")
					->addInput(NULL,"column[new0][variable]",'highlightGreen')
					->addInput(NULL,"column[new0][caption]")
					->addSelect(array('data'=>$columnTypes),"column[new0][type]")
					->addInput(NULL,"column[new0][expression]")
					->addInput(NULL,"column[new0][defaultValue]")
					->addInput(NULL,"column[new0][validRange]")
					->addInput(NULL,"column[new0][validRegex]")
					->addInput(NULL,"column[new0][validValues]");
				echo $table;
				break;
			case 'parts':
				echo '<ul class="partsList">';
				foreach($this->parts as $part) {
					echo '<li><a href="?id='.$ID.'&do='.'admin'.'&page='.'class2_main'.'&part='.$part->name().'">'.$part->name().'</a></li>';
				}
				echo '</ul>';
				break;
			case 'courses':
			default:
				echo '<ul class="coursesList">';
				foreach($this->courses as $course) {
					echo '<li><a href="?id='.$ID.'&do='.'admin'.'&page='.'class2_main'.'&course='.$course->code().'">'.$course->name().'</a></li>';
				}
				echo '</ul>';
				break;
		}
	}

}