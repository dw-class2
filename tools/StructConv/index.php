<?php
/***********************************************/
$old_filename = '../../trash/data/classification-structure.conf';
$new_filename = '../../trash/data/classification-structure.new';
/***********************************************/
use Structure\Root,
	Structure\Column,
	Structure\SerializeStorage,
	Tools\Debug;
// init simple class loader
require_once(realpath(__DIR__.'/../..').'/loader.php');
// init old stuff
require('old/_D_.php');
require('old/ClassificationStructure.php');
require('old/ClassificationStructureFileFactory.php');

echo '<pre>';
echo "convert old classification structure serialized file to new type.\n";
echo "loading old: $old_filename\n";
if(!($old = ClassificationStructureFileFactory::load($old_filename))) {
	echo "!! failed - try check filename, path and/or permission\n";
	exit;
}
echo "creating new object\n";
$new = new Root('classification structure');

echo "processing: ";

$old_lang_names = $old->getLangNames();
foreach($old_lang_names as $old_lang_name) {
	echo "L";
	//echo "$old_lang_name\n";
	$old_lang = $old->getLang($old_lang_name);
	$new->addLang($old_lang_name);
	$new_lang = $new->getLang($old_lang_name);
	$old_type_names = $old_lang->getTypeNames();
	
	foreach($old_type_names as $old_type_name) {
		echo "T";
		//echo "\t$old_type_name\n";
		$old_type = $old_lang->getType($old_type_name);
		$new_lang->addType($old_type_name);
		$new_type = $new_lang->getType($old_type_name);
		$old_part_names = $old_type->getPartNames();
		
		foreach($old_part_names as $old_part_name) {
			echo "P";
			//echo "\t\t$old_part_name\n";
			$old_part = $old_type->getPart($old_part_name);
			$new_type->addPart($old_part_name);
			$new_part = $new_type->getPart($old_part_name);
			$old_columns = $old_part->getAllColumns();
			
			foreach($old_columns as $old_column) {
				echo "C";
				$new_colspec = array(
					'idx' => $old_column['idx'],
					'type' => Column::isTypeAvailable($old_column['type']) ? $old_column['type'] : 'string',
				);
				if($old_column['type'] == 'expression') {
					$new_colspec['expression'] = $old_column['default'];
				} else {
					$new_colspec['default'] = $old_column['default'];
				}
				$new_part->addColumn($old_column['name'],$old_column['caption'],$new_colspec);
			}
		}
  	}
}
echo "\nsaving new: ".$new_filename."\n";
var_dump($new);
try {
$new_data = SerializeStorage::save($new_filename,$new);
} catch (Exception $e) {
	echo "!! Exception: ".$e->getMessage()."\n!! try check filename, path and/or permission\n";
}
echo "done\n";
echo '</pre>';
?>
