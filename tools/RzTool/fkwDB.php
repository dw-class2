<?php
class fkwDB {
	private static $pdo = NULL;
	
	private static function readConf($fn = 'db.conf') {
		$data = file_get_contents($fn,true);
		$data = explode("\n",$data);
		foreach($data as $line) {
			$line = trim($line);
			if($line[0] == '#') continue;
			$cfg = explode('=',$line);
			$$cfg[0] = $cfg[1];
		}
		return array(
			'dsn' => "mysql:dbname=$DB_NAME;unix_socket=$DB_SOCK",
			'user' => $DB_USER,
			'password' => $DB_PASS,
			);
	}
	
	private static function connect() {
		if(!self::$pdo) {
			$conf = self::readConf();

			try {
			    self::$pdo =  new PDO($conf['dsn'], $conf['user'], $conf['password']);
				self::$pdo->query('SET NAMES "utf8"');
			} catch (PDOException $e) {
			    echo 'Connection failed: ' . $e->getMessage();
			}
		}
	}
	
	public static function getData($table,$where=array()) {
		if(is_array($where) && count($where)) {
			$whereStr = '';
			foreach($where as $k=>$v) {
				$whereStr .= ($whereStr != '' ? ' AND ' : '').' '.$k.' = ?';
				$bWhere[] = $v;
			}
			$whereStr = 'WHERE '.$whereStr;
		}
		self::connect();
		$query = "SELECT * FROM $table $whereStr";
		//echo $query;
		//print_r($where);
 		//print_r($bWhere);
		$result = self::$pdo->prepare($query);
		$result->execute($bWhere);
		$ret = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$ret[] = $row;
		}
		return $ret;
	}
	
	public static function getQuery($query,$where=array()) {
		if(is_array($where) && count($where)) {
			$whereStr = '';
			foreach($where as $k=>$v) {
				$whereStr .= ($whereStr != '' ? ' AND ' : '').' '.$k.' = ?';
				$bWhere[] = $v;
			}
			$whereStr = 'WHERE '.$whereStr;
		}
		self::connect();
		$query = $query.' '.$whereStr;
		//echo $query;
		$result = self::$pdo->prepare($query);
		$result->execute($bWhere);
		$ret = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$ret[] = $row;
		}
		return $ret;
	}

	public static function getQueryIn($query,$where=array()) {
		if(is_array($where) && count($where)==2 && count($where[1])) {
			$whereStr = '';
			foreach($where[1] as $k=>$v) {
				$whereStr .= ($whereStr != '' ? ',' : '').'?';
				$bWhere[] = $v;
			}
			$whereStr = 'WHERE '.$where[0].' IN ('.$whereStr.')';
		}
		self::connect();
		$query = $query.' '.$whereStr;
		//echo $query;
		//print_r($bWhere);
		$result = self::$pdo->prepare($query);
		$result->execute($bWhere);
		$ret = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$ret[] = $row;
		}
		return $ret;
	}
}
?>