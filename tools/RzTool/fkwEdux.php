<?php
class fkwEdux {
	public static function students($data,$course) {
		$r = array();
		$c = count($data);
		for($i=0;$i<$c;$i++) {
			$row = &$data[$i];
			$r[$i] = 'INSERT INTO student(login,name,p_id,c_id,l_id) VALUES '.
				"('$row[login]','$row[jmeno]',".
				($row['pno'] ? "(SELECT id FROM lesson WHERE course_code='$course' AND number='$row[pno]')," : "NULL,").
				($row['cno'] ? "(SELECT id FROM lesson WHERE course_code='$course' AND number='$row[cno]')," : "NULL,").
				($row['lno'] ? "(SELECT id FROM lesson WHERE course_code='$course' AND number='$row[lno]'));" : "NULL);");
		}
		return $r;
	}

	public static function lessons($data,$course) {
		$r = array();
		$c = count($data);
		$j = 0;
		//for($i=0;$i<$c;$i++) {
		foreach($data as $row) {
			$r[$j++] = 'INSERT INTO lesson(id,course_code,`number`,lesson_type,week_day,week_parity,time_from,time_to) VALUES'.
				"(NULL,'$course','$row[xno]','$row[typ]','$row[den_cis]','$row[sudy_lichy]','$row[hodina]','".($row['hodina']+$row['pocet_hodin'])."');";
			$r[$j++] = "INSERT INTO lesson_has_teacher(lesson_id,teacher_login) VALUES".
				"((SELECT id FROM lesson WHERE course_code='$course' AND number='$row[xno]'),'$row[login]');";
			if($row['login2']) {
				$r[$j++] = "INSERT INTO lesson_has_teacher(lesson_id,teacher_login) VALUES".
					"((SELECT id FROM lesson WHERE course_code='$course' AND number='$row[xno]'),'$row[login2]');";
			}
		}
		return $r;
	}

	public static function courses($data) {
		$r = array();
		$c = count($data);
		for($i=0;$i<$c;$i++) {
				$row = &$data[$i];
				$r[$i] = 'INSERT INTO course(code,name) VALUES'.
					"('$row[kod]','$row[nazev]');";
			}
		return $r;
	}

	public static function teachers($data) {
		$r = array();
		$c = count($data);
		for($i=0;$i<$c;$i++) {
			$row = &$data[$i];
			$r[$i] = 'INSERT INTO teacher(login,name) VALUES'.
				"('$row[login]','$row[jmeno]');";
		}
		return $r;
	}

	public static function exams($data,$do) {
		$r = array();
		$c = count($data);
		$j = 0;
		$course_code = $do[2];
		foreach($data as $row) {
			$row['zacatek'] = substr($row['zacatek'],6,4).'-'.substr($row['zacatek'],3,2).'-'.substr($row['zacatek'],0,2).' '.substr($row['zacatek'],11,5);
			$r[$j++] = 'INSERT INTO action(orig_id,course_code,teacher_login,type,date,room,name,note) VALUES'.
				"('$row[id]','$course_code','$row[login]','ZK','$row[zacatek]','$row[mistnost]','Zkouškový termín','$row[poznamka]');";
				$data2 = fkwAdapt::getStudentsByExam($row['id']);
				foreach($data2 as $row2) {
					$r[$j++] = "INSERT INTO action_has_student(action_id,student_login) VALUES".
						"((SELECT id FROM action WHERE orig_id='$row[id]' AND type='ZK'),'$row2[login]');";
				}
		}
		return $r;
	}

	public static function actions($data,$do) {
		$r = array();
		$c = count($data);
		$j = 0;
		$course_code = $do[2];
		foreach($data as $row) {
			$row['zacatek'] = substr($row['zacatek'],6,4).'-'.substr($row['zacatek'],3,2).'-'.substr($row['zacatek'],0,2).' '.substr($row['zacatek'],11,5);
			$r[$j++] = 'INSERT INTO action(orig_id,course_code,teacher_login,type,date,room,name,note) VALUES'.
				"('$row[id]','$course_code','$row[login]','JA','$row[zacatek]','$row[mistnost]','$row[nazev]','$row[poznamka]');";
				$data2 = fkwAdapt::getStudentsByAction($row['id']);
				foreach($data2 as $row2) {
					$r[$j++] = "INSERT INTO action_has_student(action_id,student_login) VALUES".
							"((SELECT id FROM action WHERE orig_id='$row[id]' AND type='JA'),'$row2[login]');";
				}
		}
		return $r;
	}

}
?>