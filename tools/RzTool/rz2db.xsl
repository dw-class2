<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" indent="no" method="text" />
	<xsl:variable name="a">'</xsl:variable>

<xsl:template match="/ROZVRH">
	<xsl:if test="KATEDRY/katedra">
		TRUNCATE TABLE katedry;
		INSERT INTO katedry(id,kod,nazev,nazev_en) VALUES
		<xsl:apply-templates select="KATEDRY/katedra"/>;
	</xsl:if>
	<xsl:if test="MISTNOSTI/mistnost">
		TRUNCATE TABLE mistnosti;
		INSERT INTO mistnosti(id,kod,katedra_id,lokalita) VALUES
		<xsl:apply-templates select="MISTNOSTI/mistnost"/>;
	</xsl:if>
	<xsl:if test="PREDMETY/predmet">
		TRUNCATE TABLE predmety;
		INSERT INTO predmety(id,kod,nazev,nazev_en,sem) VALUES
		<xsl:apply-templates select="PREDMETY/predmet"/>;
	</xsl:if>
	<xsl:if test="UCITELE/ucitel">
		TRUNCATE TABLE ucitele;
		INSERT INTO ucitele(id,login,os_cislo,jmeno) VALUES
		<xsl:apply-templates select="UCITELE/ucitel"/>;
	</xsl:if>
	<xsl:if test="LISTKY/listek">
		TRUNCATE TABLE hodiny;
		INSERT INTO hodiny(id,ucitel_id,ucitel2_id,predmet_id,typ,xno,sudy_lichy,den_cis,hodina,pocet_hodin) VALUES
		<xsl:apply-templates select="LISTKY/listek"/>;
	</xsl:if>
	<xsl:if test="STUDENTI/student">
		TRUNCATE TABLE studenti;
		INSERT INTO studenti(id,login,os_cislo,jmeno,rocnik,skupina,stud_id) VALUES
		<xsl:apply-templates select="STUDENTI/student"/>;
	</xsl:if>
	<xsl:if test="LISTKY_STUDENTU/listek_studenta">
		TRUNCATE TABLE studenti_hodiny;
		INSERT INTO studenti_hodiny(student_id,hodina_id) VALUES
		<xsl:apply-templates select="LISTKY_STUDENTU/listek_studenta"/>;
	</xsl:if>
	<xsl:if test="ZAPISY_PREDMETU/zapis">
		TRUNCATE TABLE studenti_predmety;
		INSERT INTO studenti_predmety(student_id,predmet_id,sem) VALUES
		<xsl:apply-templates select="ZAPISY_PREDMETU/zapis"/>;
	</xsl:if>
	<xsl:if test="VYPSANE_TERMINY/termin">
		TRUNCATE TABLE terminy_zk;
		INSERT INTO terminy_zk(id,vypsal_id,predmet_id,sem,zacatek,konec,mistnost,poznamka) VALUES
		<xsl:apply-templates select="VYPSANE_TERMINY/termin"/>;
	</xsl:if>
	<xsl:if test="ZAPISY_NA_ZKOUSKY/zapis_na_zkousku">
		TRUNCATE TABLE zapisy_zk;
		INSERT INTO zapisy_zk(stud_id,termin_zk_id) VALUES
		<xsl:apply-templates select="ZAPISY_NA_ZKOUSKY/zapis_na_zkousku"/>;
	</xsl:if>
	<xsl:if test="ZKOUSEJICI/zkousejici_predmet">
		TRUNCATE TABLE zkousejici;
		INSERT INTO zkousejici(ucitel_id,predmet_id,sem) VALUES
		<xsl:apply-templates select="ZKOUSEJICI/zkousejici_predmet"/>;
	</xsl:if>
	<xsl:if test="JEDNORAZOVE_TERMINY/jednorazova_akce">
		TRUNCATE TABLE terminy_ja;
		INSERT INTO terminy_ja(id,katedra_id,vypsal_id,predmet_id,sem,nazev,zacatek,uzaverka,mistnost,poznamka) VALUES
		<xsl:apply-templates select="JEDNORAZOVE_TERMINY/jednorazova_akce"/>;
	</xsl:if>
	<xsl:if test="ZAPISY_JA/zapis_jednorazovy_termin">
		TRUNCATE TABLE zapisy_ja;
		INSERT INTO zapisy_ja(stud_id,termin_ja_id) VALUES
		<xsl:apply-templates select="ZAPISY_JA/zapis_jednorazovy_termin"/>;
	</xsl:if>
</xsl:template>

<xsl:template match="katedra">(<xsl:value-of select="concat($a,@id,$a,',',$a,@kod,$a,',',$a,@nazev_cz,$a,',',$a,@nazev_en,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="mistnost">(<xsl:value-of select="concat($a,@id,$a,',',$a,@cislo,$a,',',$a,@kat_id,$a,',',$a,@lok,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="predmet">
	(<xsl:value-of select="concat($a,@id,$a,',',$a,@kod,$a,',',$a,@nazev,$a,',',$a,@nazev_an,$a,',',$a,@sem_id,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if>
</xsl:template>

<xsl:template match="ucitel">
	(<xsl:value-of select="concat($a,@id,$a,',',$a,@login,$a,',',$a,@osoba_eid,$a,',',$a,@prijmeni,' ',@jmeno,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if>
</xsl:template>

<xsl:template match="listek[@typ_vyuky='P']">(<xsl:value-of select="concat($a,@id,$a,',',$a,@ucitel1_id,$a,',',$a,@ucitel2_id,$a,',',$a,@predmet_id,$a,',',$a,'P',$a,',',$a,@pno,$a,',',$a,@sudy_lichy,$a,',',$a,@den_cis,$a,',',$a,@hodina,$a,',',$a,@pocet_hodin,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>
<xsl:template match="listek[@typ_vyuky='C']">(<xsl:value-of select="concat($a,@id,$a,',',$a,@ucitel1_id,$a,',',$a,@ucitel2_id,$a,',',$a,@predmet_id,$a,',',$a,'C',$a,',',$a,@cno,$a,',',$a,@sudy_lichy,$a,',',$a,@den_cis,$a,',',$a,@hodina,$a,',',$a,@pocet_hodin,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>
<xsl:template match="listek[@typ_vyuky='L']">(<xsl:value-of select="concat($a,@id,$a,',',$a,@ucitel1_id,$a,',',$a,@ucitel2_id,$a,',',$a,@predmet_id,$a,',',$a,'L',$a,',',$a,@lno,$a,',',$a,@sudy_lichy,$a,',',$a,@den_cis,$a,',',$a,@hodina,$a,',',$a,@pocet_hodin,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="student">
	<!--xsl:variable name="jmeno">
	  <xsl:call-template name="string-replace-all">
	    <xsl:with-param name="text" select="concat(@prijmeni,' ',@jmeno)"/>
	    <xsl:with-param name="replace">'</xsl:with-param>
	    <xsl:with-param name="by">\'</xsl:with-param>
	  </xsl:call-template>
	</xsl:variable-->
	(<xsl:value-of select="concat($a,@id,$a,',',$a,@login,$a,',',$a,@osoba_eid,$a,',',$a,@prijmeni,' ',@jmeno,$a,',',$a,@rocnik,$a,',',$a,@skupina,$a,',',$a,@stud_id,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if>
</xsl:template>

<xsl:template match="listek_studenta">(<xsl:value-of select="concat($a,@student_id,$a,',',$a,@listek_id,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="zapis">(<xsl:value-of select="concat($a,@student_id,$a,',',$a,@predmet_id,$a,',',$a,@sem_id,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="termin">(<xsl:value-of select="concat($a,@id,$a,',',$a,@vypsal_id,$a,',',$a,@predmet_id,$a,',',$a,@sem_id,$a,',',$a,@zacatek,$a,',',$a,@konec,$a,',',$a,@misto_id,$a,',',$a,@poznamka,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="zapis_na_zkousku">(<xsl:value-of select="concat($a,@student_id,$a,',',$a,@term_id,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="zkousejici_predmet">(<xsl:value-of select="concat($a,@osoba_id,$a,',',$a,@predmet_id,$a,',',$a,@sem_id,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="jednorazova_akce">(<xsl:value-of select="concat($a,@id,$a,',',$a,@katedra_id,$a,',',$a,@vypsal_id,$a,',',$a,@predmet_id,$a,',',$a,@sem_id,$a,',',$a,@nazev,$a,',',$a,@datum,' ',@cas,$a,',',$a,@uzaverka,$a,',',$a,@misto_id,$a,',',$a,@poznamka,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<xsl:template match="zapis_jednorazovy_termin">(<xsl:value-of select="concat($a,@student_id,$a,',',$a,@termin_id,$a)"/>)<xsl:if test="position() &lt; last()">,</xsl:if></xsl:template>

<!-- http://stackoverflow.com/questions/1103205/xslt-replace-apostrophe-with-escaped-text-in-output -->
<xsl:template name="string-replace-all">
  <xsl:param name="text"/>
  <xsl:param name="replace"/>
  <xsl:param name="by"/>
  <xsl:choose>
    <xsl:when test="contains($text,$replace)">
      <xsl:value-of select="substring-before($text,$replace)"/>
      <xsl:value-of select="$by"/>
      <xsl:call-template name="string-replace-all">
        <xsl:with-param name="text" select="substring-after($text,$replace)"/>
        <xsl:with-param name="replace" select="$replace"/>
        <xsl:with-param name="by" select="$by"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$text"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>