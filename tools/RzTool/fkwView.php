<?php
class fkwView {
	public static function htmlTable($data) {
		$c = count($data);
		if(!is_array($data)) return;
		if(is_array($data[0])) {
			$r =
			"<table border=\"1\">\n".
			//** header **/
			"\t<thead>\n".
				"\t\t<tr>\n";
			foreach(array_keys($data[0]) as $th) {
					$r .= "\t\t\t<th>$th</th>\n";
			}
			$r .=
				"\t\t</tr>\n".
			"\t</thead>\n";
		}
		/** body **/
		$r .=
		"\t<tbody>\n";
		foreach($data as $tr) {
			$r .= "\t\t<tr>\n";
			foreach($tr as $td) {
				$r .= "\t\t\t<td>$td</td>\n";
			}
			$r .= "\t\t</tr>\n";
		}
		$r .=
		"\t</tbody>\n".
		"</table>\n";
		$r .= "<p>rows: $c</p>";
		return $r;
	}

	public static function dump($data) {
		$r = '<pre>'.print_r($data,true).'</pre>';
//		echo $r;
		return $r;
	}

	public static function myPrint($data,$len,$delim = '') {
		$l = mb_strlen($data,'utf8');
		echo $data.($len>$l ? str_repeat(' ',$len-$l) : '').$delim;
	}

	public static function txtTable($data) {
		if(!is_array($data)) return;
		if(is_array($data[0])) {
			/** init by header's length **/
			foreach(array_keys($data[0]) as $th) {
				$max[$th] = mb_strlen($th,'utf8');
			}
			/** count max column length **/
			foreach($data as $tr) {
				foreach($tr as $th => $td) {
					if(($l = mb_strlen($td,'utf8'))>$max[$th]) {
						$max[$th] = $l;
					}
					$c++;
				}
			}
			/** print headers **/
			foreach(array_keys($data[0]) as $th) {
				self::myPrint($th,$max[$th],' | ');
				$c++;
			}
			echo "\n";
			/** print data **/
			foreach($data as $tr) {
				foreach($tr as $th => $td) {
					self::myPrint($td,$max[$th],' | ');
				}
				echo "\n";
			}

		}
	}

	public static function UnHtml($str) {
		$lines = explode("\n",$str);
		foreach($lines as &$line) {
			$line = trim($line);
		}
		$str = str_replace("\n\n","\n",strtr(implode(" ",$lines),array(
			'<html>' => '',
			'</html>' => '',
			'<body>' => '',
			'</body>' => '',
			'<ul>' => "\n",
			'</ul>' => '',
			'<li>' => "\t* ",
			'</li>' => "\n",
			'<pre>' => '',
			'</pre>' => '',
			'<br />' => "\n",
			'<table border="1">' => '',
			'</table>' => '',
			'<thead>' => '',
			'</thead>' => '',
			'<tbody>' => '',
			'</tbody>' => '',
			'<tfoot>' => '',
			'</tfoot>' => '',
			'<tr>' => '',
			'</tr>' => "\n",
			'<td>' => '',
			'</td>' => "\t",
			'<th>' => '[',
			'</th>' => "]\t",
			'<h2>' => "\n== ",
			'</h2>' => " ==\n",
			'<em>' => '<',
			'</em>' => '>',
			'<u>' => '__',
			'</u>' => '__',
			)));
	return $str;
	}
}
?>