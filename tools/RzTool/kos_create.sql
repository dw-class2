# Sequel Pro dump
# Version 2492
# http://code.google.com/p/sequel-pro
#
# Host: localhost (MySQL 5.1.39)
# Database: kos
# Generation Time: 2011-02-16 23:25:35 +0100
# ************************************************************

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table hodiny
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hodiny`;

CREATE TABLE `hodiny` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `ucitel_id` bigint(11) DEFAULT NULL,
  `ucitel2_id` bigint(11) DEFAULT NULL,
  `predmet_id` bigint(11) DEFAULT NULL,
  `typ` varchar(1) DEFAULT NULL,
  `xno` int(6) DEFAULT NULL,
  `sudy_lichy` varchar(1) DEFAULT NULL,
  `den_cis` int(1) DEFAULT NULL,
  `hodina` int(2) DEFAULT NULL,
  `pocet_hodin` int(1) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `ucitel_id` (`ucitel_id`),
  KEY `predmet_id` (`predmet_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table katedry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `katedry`;

CREATE TABLE `katedry` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `kod` int(11) DEFAULT NULL,
  `nazev` varchar(64) DEFAULT NULL,
  `nazev_en` varchar(64) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table mistnosti
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mistnosti`;

CREATE TABLE `mistnosti` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `kod` varchar(20) DEFAULT NULL,
  `katedra_id` bigint(11) DEFAULT NULL,
  `lokalita` varchar(10) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table predmety
# ------------------------------------------------------------

DROP TABLE IF EXISTS `predmety`;

CREATE TABLE `predmety` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `kod` varchar(16) DEFAULT NULL,
  `nazev` varchar(64) DEFAULT NULL,
  `nazev_en` varchar(64) DEFAULT NULL,
  `sem` varchar(6) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `kod` (`kod`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table studenti
# ------------------------------------------------------------

DROP TABLE IF EXISTS `studenti`;

CREATE TABLE `studenti` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `login` varchar(10) DEFAULT NULL,
  `os_cislo` int(11) DEFAULT NULL,
  `jmeno` varchar(64) DEFAULT NULL,
  `rocnik` int(2) DEFAULT NULL,
  `skupina` int(4) DEFAULT NULL,
  `stud_id` bigint(11) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `login` (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table studenti_hodiny
# ------------------------------------------------------------

DROP TABLE IF EXISTS `studenti_hodiny`;

CREATE TABLE `studenti_hodiny` (
  `student_id` bigint(11) NOT NULL DEFAULT '0',
  `hodina_id` bigint(11) NOT NULL DEFAULT '0',
  KEY `ids` (`student_id`,`hodina_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table studenti_predmety
# ------------------------------------------------------------

DROP TABLE IF EXISTS `studenti_predmety`;

CREATE TABLE `studenti_predmety` (
  `student_id` bigint(11) NOT NULL DEFAULT '0',
  `predmet_id` bigint(11) NOT NULL DEFAULT '0',
  `sem` varchar(6) DEFAULT NULL,
  KEY `ids` (`student_id`,`predmet_id`),
  KEY `ids2` (`predmet_id`,`student_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table terminy_ja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `terminy_ja`;

CREATE TABLE `terminy_ja` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `katedra_id` bigint(11) DEFAULT NULL,
  `vypsal_id` bigint(11) DEFAULT NULL,
  `predmet_id` bigint(11) DEFAULT NULL,
  `sem` varchar(6) DEFAULT NULL,
  `nazev` varchar(64) DEFAULT NULL,
  `zacatek` varchar(16) DEFAULT NULL,
  `uzaverka` varchar(10) DEFAULT NULL,
  `mistnost` bigint(11) DEFAULT NULL,
  `poznamka` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vypsal_id` (`vypsal_id`),
  KEY `predmet_id` (`predmet_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table terminy_zk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `terminy_zk`;

CREATE TABLE `terminy_zk` (
  `id` bigint(32) NOT NULL DEFAULT '0',
  `vypsal_id` bigint(11) DEFAULT NULL,
  `predmet_id` bigint(11) DEFAULT NULL,
  `sem` varchar(6) DEFAULT NULL,
  `zacatek` varchar(16) DEFAULT NULL,
  `konec` varchar(16) DEFAULT NULL,
  `mistnost` bigint(11) DEFAULT NULL,
  `poznamka` varchar(255) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `vypsal_id` (`vypsal_id`),
  KEY `predmet_id` (`predmet_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table ucitele
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ucitele`;

CREATE TABLE `ucitele` (
  `id` bigint(11) NOT NULL DEFAULT '0',
  `login` varchar(10) DEFAULT NULL,
  `os_cislo` int(11) DEFAULT NULL,
  `jmeno` varchar(64) DEFAULT NULL,
  KEY `id` (`id`),
  KEY `login` (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table zapisy_ja
# ------------------------------------------------------------

DROP TABLE IF EXISTS `zapisy_ja`;

CREATE TABLE `zapisy_ja` (
  `stud_id` bigint(11) NOT NULL DEFAULT '0',
  `termin_ja_id` bigint(11) NOT NULL DEFAULT '0',
  KEY `ids` (`stud_id`,`termin_ja_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table zapisy_zk
# ------------------------------------------------------------

DROP TABLE IF EXISTS `zapisy_zk`;

CREATE TABLE `zapisy_zk` (
  `stud_id` bigint(11) NOT NULL DEFAULT '0',
  `termin_zk_id` bigint(11) NOT NULL DEFAULT '0',
  KEY `ids` (`stud_id`,`termin_zk_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table zkousejici
# ------------------------------------------------------------

DROP TABLE IF EXISTS `zkousejici`;

CREATE TABLE `zkousejici` (
  `ucitel_id` bigint(11) NOT NULL DEFAULT '0',
  `predmet_id` bigint(11) NOT NULL DEFAULT '0',
  `sem` varchar(6) DEFAULT NULL,
  KEY `ids` (`predmet_id`,`ucitel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;






/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
