<?php
define('FKW_VERSION',0.11);
/*** 
 *** fkw 0.11 = fool kos wrapper/webservice? 
 *** jirkovoj@fit.cvut.cz
 *** see demo.php for how-to use it internally
 ***/
require('fkwCmd.php');
require('fkwDB.php');
require('fkwAdapt.php');
require('fkwEdux.php');
require('fkwView.php');

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

$time_start = microtime_float();

/*** preprocess query ***/
if(PHP_SAPI === 'cli') {
	$query = $argv[1];
	$mode = 'cli';
} else {
	$query = $_SERVER['REDIRECT_QUERY_STRING'];
	$mode = 'web';
}
$do = fkwCommand::prepare($query);
$edux = fkwCommand::edux();
if($edux) {
	$mode = 'edux';
}
/*** generate html header if needed ***/
if(!$edux && $mode !== 'cli') {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>fkw <?php echo FKW_VERSION ?></title>
</head>
<body>
<?php
}
/*** execute command ***/
$r = fkwCommand::command($do);
/*** view ***/
switch($mode) {
	case 'edux':
		header('Content-type: text/plain');
		foreach($r as $i) {
			echo $i."\n";
		}
		break;
	case 'web':
		echo fkwView::htmlTable($r);
		break;
	case 'cli':
		echo fkwView::txtTable($r);
		break;
	default:
		fkwCommand::info();
}
/*** generate html footer if needed ***/
if(!$edux) {
	$time_end = microtime_float();
	$time = $time_end - $time_start;
	if($mode == 'web') {
		echo "<p>working time: $time</p>";
	} else {
		echo "working time: $time\n";
	}
}
if($mode == 'web') {
?>
</body>
</html>
<?php
}
?>