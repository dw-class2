<?php
class fkwCommand {
	private static $edux = false;

	public static function edux() {
		return self::$edux;
	}

	public static function prepare($query) {
		$killtable = array(
			'\'' => '',
			'"' => '',
			'\\' => '',
			'&apos;' => '',
			'&quot;' => '',
			'&amp;apos;' => '',
			'&amp;quot;' => '',
			'%27' => '', //apos
			'%22' => '', //quot
			'%5C' => '', //backslash
		);
		$query = strtr($query,$killtable);

		if(substr($query,-1,1) == '/') {
			$query = substr($query,0,-1);
		}
		$do = explode('/',$query);
		$c = count($do);
		/*** edux mode ***/
		if($do[$c-1] == 'edux') {
			array_pop($do);
			self::$edux = true;
		}
		return $do;
	}

	public static function command($do) {
		$r = array();
		switch($do[0]) {
			case 'student':
			case 'studenti':
				$r = self::studenti($do);
				break;
			case 'ucitel':
			case 'ucitele':
				$r = self::ucitele($do);
				break;
			case 'hodiny':
				$r = self::hodiny($do);
				break;
			case 'predmet':
			case 'predmety':
				$r = self::predmety($do);
				break;
			case 'zkousky':
				$r = self::zkousky($do);
				break;
			case 'akce':
				$r = self::akce($do);
				break;
			default:
				$r = self::info();
				break;
		}

		return $r;
	}

	public static function info($cmd = NULL) {
		$r = array();
		switch($cmd) {
			case 'student':
			case 'studenti':
				$r = array(
					array(
						'cmd' => 'student/LOGIN',
						'edux' => '',
					),
					array(
						'cmd' => 'studenti/dle-predmetu/PREDMET',
						'edux' => '',
					),
					array(
						'cmd' => 'studenti/dle-predmetu/PREDMET/hodiny',
						'edux' => '/edux',
					),
					array(
						'cmd' => 'studenti/dle-zkousky/ID',
						'edux' => '',
					),
					array(
						'cmd' => 'studenti/dle-akce/ID',
						'edux' => '',
					),
				);
				break;
			case 'ucitel':
			case 'ucitele':
				$r = array(
					array(
						'cmd' => 'ucitel/LOGIN',
						'edux' => '',
					),
					array(
						'cmd' => 'ucitele/dle-predmetu/PREDMET1/PREDMET2/...',
						'edux' => '/edux',
					),
				);
				break;
			case 'hodiny':
				$r = array(
					array(
						'cmd' => 'hodiny/dle-predmetu/PREDMET',
						'edux' => '/edux',
					),
				);
				break;
			case 'predmety':
				$r = array(
					array(
						'cmd' => 'predmet/PREDMET1/PREDMET2/...',
						'edux' => '/edux',
					),
					array(
						'cmd' => 'predmety/PREDMET1/PREDMET2/...',
						'edux' => '/edux',
					),
				);
				break;
			case 'zkousky':
				$r = array(
					array(
						'cmd' => 'zkousky/dle-predmetu/PREDMET',
						'edux' => '/edux',
					),
					array(
						'cmd' => 'zkousky/dle-ucitele/LOGIN',
						'edux' => '',
					),
					array(
						'cmd' => 'zkousky/dle-studenta/PREDMET',
						'edux' => '',
					),
				);
				break;
			case 'akce':
				$r = array(
					array(
						'cmd' => 'akce/dle-predmetu/PREDMET',
						'edux' => '',
					),
					array(
						'cmd' => 'akce/dle-ucitele/LOGIN',
						'edux' => '',
					),
					array(
						'cmd' => 'akce/dle-studenta/PREDMET',
						'edux' => '',
					),
				);
				break;
			default:
				foreach(array('studenti','ucitele','hodiny','predmety','zkousky','akce') as $cmd) {
					$r = array_merge($r,self::info($cmd));
				}
				break;
		}

		return $r;
	}

	public static function studenti($do) {
		$c = count($do);
		$r = array();

		if($c == 2 && $do[0] == 'student') {
			$r = fkwAdapt::getStudentByLogin($do[1]);
		} else

		if($c == 3 && $do[0] == 'studenti' && $do[1] == 'dle-predmetu') {
			$r = fkwAdapt::getStudentsByCourse($do[2]);
		} else

		if($c == 4 && $do[0] == 'studenti' && $do[1] == 'dle-predmetu' && $do[3] == 'hodiny') {
			$r = fkwAdapt::getStudentsWithLessons($do[2]);
			if(self::$edux) {
				$r = fkwEdux::students($r,$do[2]);
			}
		} else

		if($c == 3 && $do[0] == 'studenti' && $do[1] == 'dle-zkousky') {
			$r = fkwAdapt::getStudentsByExam($do[2]);
		} else

		if($c == 3 && $do[0] == 'studenti' && $do[1] == 'dle-akce') {
			$r = fkwAdapt::getStudentsByAction($do[2]);
		} else {
			$r = self::info('studenti');
		}

		return $r;
	}

	public static function ucitele($do) {
		$c = count($do);
		$r = array();

		if($c == 2 && $do[0] == 'ucitel') {
			$r = fkwAdapt::getTeacherByLogin($do[1]);
		} else

		if($c >= 3 && $do[0] == 'ucitele' && $do[1] == 'dle-predmetu') {
			unset($do[0]);
			unset($do[1]);
			$r = fkwAdapt::getTeachersByCourses($do);
			if(self::$edux) {
				$r = fkwEdux::teachers($r);
			}
		} else {
			$r = self::info('ucitele');
		}

		return $r;
	}

	public static function hodiny($do) {
		$c = count($do);
		$r = array();

		if($c == 3 && $do[0] == 'hodiny' && $do[1] == 'dle-predmetu') {
			$r = fkwAdapt::getLessonsByCourse($do[2]);
			if(self::$edux) {
				$r = fkwEdux::lessons($r,$do[2]);
			}
		} else {
			$r = self::info('hodiny');
		}

		return $r;
	}

	public static function predmety($do) {
		$c = count($do);
		$r = array();

		if($c >= 2 && ($do[0] == 'predmet' || $do[0] == 'predmety')) {
			unset($do[0]);
			$r = fkwAdapt::getCourses($do);
			if(self::$edux) {
				$r = fkwEdux::courses($r,$do);
			}
		} else {
			$r = self::info('predmety');
		}

		return $r;
	}

	public static function zkousky($do) {
		$c = count($do);
		$r = array();

		if($c == 3 && $do[0] == 'zkousky' && $do[1] == 'dle-predmetu') {
			$r = fkwAdapt::getExamsByCourse($do[2]);
			if(self::$edux) {
				$r = fkwEdux::exams($r,$do);
			}
		} else

		if($c == 3 && $do[0] == 'zkousky' && $do[1] == 'dle-ucitele') {
			$r = fkwAdapt::getExamsByTeacher($do[2]);
		} else

		if($c == 3 && $do[0] == 'zkousky' && $do[1] == 'dle-studenta') {
			$r = fkwAdapt::getExamsByStudent($do[2]);

		} else {
			$r = self::info('zkousky');
		}

		return $r;
	}

	public static function akce($do) {
		$c = count($do);
		$r = array();

		if($c == 3 && $do[0] == 'akce' && $do[1] == 'dle-predmetu') {
			$r = fkwAdapt::getActionsByCourse($do[2]);
			if(self::$edux) {
				$r = fkwEdux::actions($r,$do);
			}
		} else

		if($c == 3 && $do[0] == 'akce' && $do[1] == 'dle-ucitele') {
			$r = fkwAdapt::getActionsByTeacher($do[2]);
		} else

		if($c == 3 && $do[0] == 'akce' && $do[1] == 'dle-studenta') {
			$r = fkwAdapt::getActionsByStudent($do[2]);
		} else {
			$r = self::info('akce');
		}

		return $r;
	}

}
?>