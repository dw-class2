<?php
class fkwAdapt {
	public static function getStudentByLogin($login) {
		return fkwDB::getData('studenti',array('login' => $login));
	}

	public static function getStudentsByCourse($course) {
		return fkwDB::getQuery('SELECT s.* FROM studenti AS s LEFT JOIN studenti_predmety AS sp ON s.id = sp.student_id LEFT JOIN predmety AS p ON p.id = sp.predmet_id',array('p.kod'=>$course));
	}

	public static function getStudentsWithLessons($course) {
		return fkwDB::getQuery('SELECT DISTINCT s.login,s.jmeno,'.
		'(SELECT h.xno FROM hodiny AS h LEFT JOIN studenti_hodiny AS sh ON h.id = sh.hodina_id WHERE h.predmet_id = (SELECT id FROM predmety WHERE kod=\''.$course.'\') AND s.id=sh.student_id AND h.typ=\'P\' AND xno IS NOT NULL) as pno,'.
		'(SELECT h.xno FROM hodiny AS h LEFT JOIN studenti_hodiny AS sh ON h.id = sh.hodina_id WHERE h.predmet_id = (SELECT id FROM predmety WHERE kod=\''.$course.'\') AND s.id=sh.student_id AND h.typ=\'C\' AND xno IS NOT NULL) as cno,'.
		'(SELECT h.xno FROM hodiny AS h LEFT JOIN studenti_hodiny AS sh ON h.id = sh.hodina_id WHERE h.predmet_id = (SELECT id FROM predmety WHERE kod=\''.$course.'\') AND s.id=sh.student_id AND h.typ=\'L\' AND xno IS NOT NULL) as lno '.
		'FROM studenti AS s LEFT JOIN studenti_predmety AS sp ON s.id=sp.student_id LEFT JOIN predmety AS p ON sp.predmet_id=p.id',array('p.kod'=>$course));
	}

	public static function getLessonsByCourse($course) {
		return fkwDB::getQuery('SELECT xno,typ,sudy_lichy,den_cis,hodina,pocet_hodin,u.login AS login,u.jmeno AS jmeno,u2.login AS login2,u2.jmeno AS jmeno2 FROM hodiny AS h LEFT JOIN ucitele AS u ON h.ucitel_id=u.id LEFT JOIN ucitele AS u2 ON h.ucitel2_id=u2.id WHERE predmet_id = (SELECT id FROM predmety WHERE kod=\''.$course.'\');');
	}

	public static function getCourses($courses) {
		if(!is_array($courses)) {
			$courses = array($courses);
		}
		return fkwDB::getQueryIn('SELECT kod,nazev FROM predmety',array('kod',$courses));
	}

	public static function getTeacherByLogin($login) {
		return fkwDB::getData('ucitele',array('login' => $login));
	}

	public static function getTeachersByCourses($courses) {
		if(!is_array($courses)) {
			$courses = array($courses);
		}
		$whereStr = '';
		foreach($courses as $v) {
			$whereStr .= ($whereStr != '' ? ',' : '')."'$v'";
		}
		return fkwDB::getQuery('SELECT DISTINCT login,jmeno FROM ucitele AS u LEFT JOIN hodiny AS h ON (u.id=h.ucitel_id OR u.id=h.ucitel2_id) WHERE h.predmet_id IN (SELECT id FROM predmety WHERE kod IN ('.$whereStr.'));');
	}

	public static function getExamsByCourse($course) {
		return fkwDB::getQuery('SELECT t.id,t.zacatek,m.kod AS mistnost,u.login,u.jmeno,t.poznamka FROM terminy_zk AS t LEFT JOIN ucitele AS u ON t.vypsal_id=u.id LEFT JOIN mistnosti AS m ON t.mistnost=m.id WHERE predmet_id = (SELECT id FROM predmety WHERE kod=\''.$course.'\');');
	}

	public static function getExamsByTeacher($login) {
		return fkwDB::getQuery('SELECT t.id,p.kod,p.nazev,t.zacatek,m.kod AS mistnost,t.poznamka FROM terminy_zk AS t LEFT JOIN predmety AS p ON t.predmet_id=p.id LEFT JOIN mistnosti AS m ON t.mistnost=m.id WHERE vypsal_id = (SELECT id FROM ucitele WHERE login=\''.$login.'\');');
	}

	public static function getExamsByStudent($login) {
		return fkwDB::getQuery('SELECT p.kod,u.login,u.jmeno,p.nazev,t.zacatek,m.kod AS mistnost,t.poznamka FROM terminy_zk AS t LEFT JOIN predmety AS p ON t.predmet_id=p.id LEFT JOIN mistnosti AS m ON t.mistnost=m.id LEFT JOIN ucitele AS u ON t.vypsal_id=u.id LEFT JOIN zapisy_zk AS z ON z.termin_zk_id=t.id LEFT JOIN studenti AS s ON z.stud_id=s.stud_id WHERE s.login=\''.$login.'\';');
	}

	public static function getStudentsByExam($id) {
		return fkwDB::getQuery('SELECT s.login,s.jmeno FROM zapisy_zk AS z LEFT JOIN studenti AS s ON z.stud_id=s.stud_id WHERE z.termin_zk_id=\''.$id.'\';');
	}

	public static function getActionsByCourse($course) {
		return fkwDB::getQuery('SELECT t.id,t.zacatek,m.kod AS mistnost,t.nazev,u.login,u.jmeno,t.poznamka FROM terminy_ja AS t LEFT JOIN ucitele AS u ON t.vypsal_id=u.id LEFT JOIN mistnosti AS m ON t.mistnost=m.id WHERE predmet_id = (SELECT id FROM predmety WHERE kod=\''.$course.'\');');
	}

	public static function getActionsByTeacher($login) {
		return fkwDB::getQuery('SELECT t.id,p.kod,t.zacatek,m.kod AS mistnost,t.nazev,t.poznamka FROM terminy_ja AS t LEFT JOIN predmety AS p ON t.predmet_id=p.id LEFT JOIN mistnosti AS m ON t.mistnost=m.id WHERE vypsal_id = (SELECT id FROM ucitele WHERE login=\''.$login.'\');');
	}

	public static function getStudentsByAction($id) {
		return fkwDB::getQuery('SELECT s.login,s.jmeno FROM zapisy_ja AS z LEFT JOIN studenti AS s ON z.stud_id=s.stud_id WHERE z.termin_ja_id=\''.$id.'\';');
	}
	
	public static function getActionsByStudent($login) {
		return fkwDB::getQuery('SELECT p.kod,u.login,u.jmeno,p.nazev,t.nazev,t.zacatek,m.kod AS mistnost,t.poznamka FROM terminy_ja AS t LEFT JOIN predmety AS p ON t.predmet_id=p.id LEFT JOIN mistnosti AS m ON t.mistnost=m.id LEFT JOIN ucitele AS u ON t.vypsal_id=u.id LEFT JOIN zapisy_ja AS z ON z.termin_ja_id=t.id LEFT JOIN studenti AS s ON z.stud_id=s.stud_id WHERE s.login=\''.$login.'\';');
	}	
	
}
?>