<?php
function info($cmd = NULL) {
	global $edux;

	if($edux) {
		echo 'info-'.$cmd;
		return;
	}
	switch($cmd) {
		case 'student':
		case 'studenti':
			?>
			<h2><?=$cmd?></h2>
			<ul>
				<li>student/<em>login</em></li>
				<li>studenti/dle-predmetu/<em>predmet</em></li>
				<li><u>studenti/dle-predmetu/<em>predmet</em>/hodiny</u></li>
			</ul>
			<?php
			break;
		case 'ucitel':
		case 'ucitele':
			?>
			<h2><?=$cmd?></h2>
			<ul>
				<li>ucitel/<em>login</em></li>
				<li><u>ucitele/dle-predmetu/<em>predmet1</em>/<em>predmet2</em>/...</u></li>
			</ul>
			<?php
			break;
		case 'hodiny':
			?>
			<h2><?=$cmd?></h2>
			<ul>
				<li><u>hodiny/dle-predmetu/<em>predmet</em></u></li>
			</ul>
			<?php
			break;
		case 'predmety':
			?>
			<h2><?=$cmd?></h2>
			<ul>
				<li><u>predmet/<em>predmet1</em>/<em>predmet2</em>/...</u></li>
				<li><u>predmety/<em>predmet1</em>/<em>predmet2</em>/...</u></li>
			</ul>
			<?php
			break;
		default:
			foreach(array('studenti','ucitele','hodiny','predmety') as $cmd) {
				info($cmd);
			}
			break;
	}
}

function studenti($do) {
	global $edux;
	$c = count($do);
	$data = array();

	if($c == 2 && $do[0] == 'student') {
		$data = fkwAdapt::getStudentByLogin($do[1]);
	} else

	if($c == 3 && $do[0] == 'studenti' && $do[1] == 'dle-predmetu') {
		$data = fkwAdapt::getStudentsByCourse($do[2]);
	} else

	if($c == 4 && $do[0] == 'studenti' && $do[1] == 'dle-predmetu' && $do[3] == 'hodiny') {
		$data = fkwAdapt::getStudentsWithLessons($do[2]);
		if($edux) {
			fkwEdux::Students($data,$do[2]);
		}
	} else {
		info('studenti');
		return;
	}

	if(!$edux) {
		fkwView::Table($data);
	}
}

function ucitele($do) {
	global $edux;
	$c = count($do);
	$data = array();

	if($c == 2 && $do[0] == 'ucitel') {
		$data = fkwAdapt::getTeacherByLogin($do[1]);
	} else

	if($c >= 3 && $do[0] == 'ucitele' && $do[1] == 'dle-predmetu') {
		unset($do[0]);
		unset($do[1]);
		$data = fkwAdapt::getTeachersByCourses($do);
		if($edux) {
		fkwEdux::Teachers($data);
		}
	} else {
		info('ucitele');
		return;
	}

	if(!$edux) {
		fkwView::Table($data);
	}
}

function hodiny($do) {
	global $edux;
	$c = count($do);
	$data = array();

	if($c == 3 && $do[0] == 'hodiny' && $do[1] == 'dle-predmetu') {
		$data = fkwAdapt::getLessonsByCourse($do[2]);
		if($edux) {
			fkwEdux::Lessons($data,$do[2]);
		}
	} else {
		info('hodiny');
		return;
	}

	if(!$edux) {
		fkwView::Table($data);
	}
}

function predmety($do) {
	global $edux;
	$c = count($do);
	$data = array();

	if($c >= 2 && ($do[0] == 'predmet' || $do[0] == 'predmety')) {
		unset($do[0]);
		$data = fkwAdapt::getCourses($do);
		if($edux) {
			fkwEdux::Courses($data,$do);
		}
	} else {
		info('predmety');
		return;
	}

	if(!$edux) {
		fkwView::Table($data);
	}
}
?>