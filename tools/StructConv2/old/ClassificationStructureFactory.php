<?php
require_once('_D_.php');
require_once('ClassificationStructure.php');
/**
 * ClassificationStructureFactory - loads/saves structure storage <--> ClassificationStructureRoot
 */

abstract class ClassificationStructureFactory {
/**
 * Load - loads ClassificationStructureRoot from storage
 * @return object ClassificationStructureRoot - loaded structure
 * @param object $storage - identifies storage: file, database connection...
 */
	abstract public static function &Load($storage);
/**
 * Save - saves ClassificationStructureRoot to storage
 * @return nothing
 * @param object $storage - identifies storage: file, database connection...
 * @param object ClassificationStructureRoot $csr - structure to be saved
 */
	abstract public static function Save($storage,$csr);
}
?>