<?php
require_once('ClassificationStructureFactory.php');
/**
 * ClassificationStructureFileFactory - loads/saves structure file <--> ClassificationStructureRoot
 */

class ClassificationStructureFileFactory extends ClassificationStructureFactory {
	private static $FileCache;
/**
 * Load - loads ClassificationStructureRoot from file
 * @return object ClassificationStructureRoot - loaded structure
 * @param object $file - identifies file: file, database connection...
 */
	public static function &Load($file) {
		_d_dbg(__CLASS__."->Load: file=$file");
		_d_chkspec($file,'file',__CLASS__.'->Load');
		if(!self::$FileCache[$file]) {
			$str = @file_get_contents($file);
			if($str === false) {
				self::$FileCache[$file] = NULL;
			} else {
				self::$FileCache[$file] = unserialize($str);
			}
		}
		return self::$FileCache[$file];
	}
/**
 * Save - saves ClassificationStructureRoot to file
 * @return nothing
 * @param object $file - identifies file: file, database connection...
 * @param object ClassificationStructureRoot $csr - structure to be saved
 */
	public static function Save($file,$csr) {
		_d_dbg(__CLASS__."->Save: file=$file, csr=$csr");
		_d_chkspec($file,'file',__CLASS__.'->Save');
		$str = serialize($csr);
		if(@file_put_contents($file,$str) === false) {
			throw new _D_FileIOException(0, __CLASS__.'->Save: structure save failed');
		}
	}
}
?>