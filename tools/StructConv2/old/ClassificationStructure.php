<?php
/**
 * Model of classification structure (more classes)
 */
require_once('_D_.php');
/**
 * ClassificationStructureRoot - class representing root of classification structure hierarchy, holds languages
 */
class ClassificationStructureRoot {
/**
 * @var Langs - array of ClassificationStructureLang objects, represents available languages for this structure
 */
	private $Langs;
/**
 * __construct
 * @return nothing
 * @param string $lang[optional] - name of new language
 * @param string $type[optional] - name of new type of given language
 * @param string $part[optional] - name of new part of given type
 */
	public function __construct($lang = NULL,$type = NULL,$part = NULL) {
		_d_dbg(get_class($this)."->__construct: lang=$lang, type=$type, part=$part");
		// adding first language is optional
		if(!empty($lang)) {
			$this->addLang($lang,$type,$part);
		}
	}

	public function __toString() {
		return _D_BEGIN_MSG.print_r($this,true)._D_END_MSG;
	}
/**
 * addLang - adds new language
 * @return nothing
 * @param string $lang - name of new language
 * @param string $type[optional] - name of new type of given language
 * @param string $part[optional] - name of new part of given type
 */
	public function addLang($lang = NULL,$type = NULL,$part = NULL) {
		_d_dbg(get_class($this)."->addLang: lang=$lang, type=$type, part=$part");
		//check if lang is specified was left to constructor
		_d_chknotexist($this->Langs[$lang],'lang',get_class($this).'->addLang');
		$this->Langs[$lang] = new ClassificationStructureLang($lang,$type,$part);
	}
/**
 * modLang - renames language
 * @return nothing
 * @param string $lang - original name of language
 * @param string $lang_new - new name of language
 */
	public function modLang($lang = NULL,$lang_new = NULL) {
		_d_dbg(get_class($this)."->modLang: lang=$lang, lang_new=$lang_new");
		_d_chkspec($lang,'lang',get_class($this).'->modLang');
		_d_chkspec($lang_new,'new lang',get_class($this).'->modLang');
		_d_chkexist($this->Langs[$lang],'lang',get_class($this).'->modLang');
		_d_chknotexist($this->Langs[$lang_new],'new lang',get_class($this).'->modLang');
		$this->Langs[$lang_new] = &$this->Langs[$lang];
		//don't delete sub-structure
		unset($this->Langs[$lang]);
		$this->Langs[$lang_new]->setName($lang_new);
	}
/**
 * delLang - deletes language
 * @return nothing
 * @param string $lang - name of language
 */
	public function delLang($lang = NULL) {
		_d_dbg(get_class($this)."->delLang: lang=$lang");
		_d_chkspec($lang,'lang',get_class($this).'->delLang');
		_d_chkexist($this->Langs[$lang],'lang',get_class($this).'->delLang');
		$this->Langs[$lang]->delAllTypes();
		unset($this->Langs[$lang]);
	}
/**
 * delAllLangs - deletes all languages
 * @return nothing
 */
	public function delAllLangs() {
		_d_dbg(get_class($this)."->delAllLangs");
		if(!empty($this->Langs)) {
			foreach($this->Langs as $langname => $lang) {
				$this->delLang($langname);
			}
		}
	}
/**
 * getLang - return language by given name if exists
 * @return object ClassificationStructureLang - return language by given name if exists
 * @param string $lang - name of language
 */
	public function getLang($lang) {
		_d_dbg(get_class($this)."->getLang: lang=$lang");
		_d_chkspec($lang, 'lang', get_class($this).'->getLang');
		//_d_chkexist($this->Langs[$lang], 'lang', get_class($this).'->getLang');
		return $this->Langs[$lang];
	}
/**
 * getLangNames - return array of language names
 * @return array of language names
 */
	public function getLangNames() {
		return empty($this->Langs) ? array() : array_keys($this->Langs);
	}
}
/**
 * ClassificationStructureLang - class representing classification structure language, holds study types
 */
class ClassificationStructureLang {
/**
 * @var Name - name of language
 */
	private $Name;
/**
 * @var Types - array of ClassificationStructureType objects, represents available types for this language
 */
	private $Types;
/**
 * __construct
 * @return nothing
 * @param string $lang - name of language
 * @param string $type[optional] - name of new type of given language
 * @param string $part[optional] - name of new part of given type
 */
	public function __construct($lang = NULL,$type = NULL,$part = NULL) {
		_d_dbg(get_class($this)."->__construct: lang=$lang, type=$type, part=$part");
		_d_chkspec($lang,'lang',get_class($this).'->__construct');
		$this->Name = $lang;
		// adding first type is optional
		if(!empty($type)) {
			$this->addType($type,$part);
		}
	}

	public function __toString() {
		return _D_BEGIN_MSG.print_r($this,true)._D_END_MSG;
	}
/**
 * addType - adds new type for this language
 * @return nothing
 * @param string $type - name of type
 * @param string $part[optional] - name of part
 */
	public function addType($type = NULL,$part = NULL) {
		_d_dbg(get_class($this)."->addType: type=$type, part=$part");
		_d_chkspec($type,'type',get_class($this).'->addType');
		_d_chknotexist($this->Types[$type],'type',get_class($this).'->addType');
		$this->Types[$type] = new ClassificationStructureType($type,$part);
	}
/**
 * modType - renames type for this language
 * @return nothing
 * @param string $type - original name of type
 * @param string $type_new - new name of type
 */
	public function modType($type = NULL,$type_new = NULL) {
		_d_dbg(get_class($this)."->modType: type=$type, type_new=$type_new");
		_d_chkspec($type,'type',get_class($this).'->modType');
		_d_chkspec($type_new,'new type',get_class($this).'->modType');
		_d_chkexist($this->Types[$type],'type',get_class($this).'->modType');
		_d_chknotexist($this->Types[$type_new],'new type',get_class($this).'->modType');
		$this->Types[$type_new] = &$this->Types[$type];
		//don't delete sub-structure
		unset($this->Types[$type]);
		$this->Types[$type_new]->setName($type_new);
	}
/**
 * delType - deletes type for this language
 * @return nothing
 * @param string $type - name of type
 */
	public function delType($type = NULL) {
		_d_dbg(get_class($this)."->delType: type=$type");
		_d_chkspec($type,'type',get_class($this).'->delType');
		_d_chkexist($this->Types[$type],'type',get_class($this).'->delType');
		$this->Types[$type]->delAllParts();
		unset($this->Types[$type]);
	}
/**
 * delAllTypes - deletes all types for this language
 * @return nothing
 */
	public function delAllTypes() {
		_d_dbg(get_class($this)."->delAllTypes");
		if(!empty($this->Types)) {
			foreach($this->Types as $typename => $type) {
				$this->delType($typename);
			}
		}
	}
	/**
 * getType - return type by given name if exists
 * @return object ClassificationStructureType - type by given name if exitsts
 * @param string $type - name of type
 */
	public function getType($type) {
		_d_dbg(get_class($this)."->getType: type=$type");
		_d_chkspec($type, 'type', get_class($this).'->getType');
		//_d_chkexist($this->Types[$type], 'type', get_class($this).'->getType');
		return $this->Types[$type];
	}
/**
 * getTypeNames - return array of type names
 * @return array of type names
 */
	public function getTypeNames() {
		return empty($this->Types) ? array() : array_keys($this->Types);
	}
/**
 * setName - magic method for internal hack of object property
 * @return nothing
 * @param object $name
 */
	public function setName($name) {
		_d_dbg(get_class($this)."->setName: name=$name");
		$this->Name = $name;
	}
/**
 * getName - magic method for steal private object property and blab it out
 * @return string - name of lang
 */
	public function getName() {
		return $this->Name;
	}
}
/**
 * ClassificationStructureType -  class representing classification structure type, holds parts
 */
class ClassificationStructureType {
/**
 * @var Name - name of type
 */
	private $Name;
/**
 * @var Parts - array of ClassificationStructurePart objects, represents available parts for this type
 */
	private $Parts;
/**
 * __construct
 * @return nothing
 * @param string $type - name of type
 * @param string $part[optional] - name of new part for given type
 */
	public function __construct($type = NULL,$part = NULL) {
		_d_dbg(get_class($this)."->__construct: type=$type, part=$part");
		_d_chkspec($type,'type',get_class($this).'->__construct');
		$this->Name = $type;
		// adding first part is optional
		if(!empty($part)) {
			$this->addPart($part);
		}
	}

	public function __toString() {
		return _D_BEGIN_MSG.print_r($this,true)._D_END_MSG;
	}
/**
 * addPart - adds new part for this type
 * @return nothing
 * @param string $part - name of part
 */
	public function addPart($part = NULL) {
		_d_dbg(get_class($this)."->addPart: part=$part");
		_d_chkspec($part,'part',get_class($this).'->addPart');
		_d_chknotexist($this->Parts[$part],'part',get_class($this).'->addPart');
		$this->Parts[$part] = new ClassificationStructurePart($part);
	}
/**
 * modPart - renames part for this type
 * @return nothing
 * @param string $part - original name of part
 * @param string $part_new - new name of part
 */
	public function modPart($part = NULL,$part_new = NULL) {
		_d_dbg(get_class($this)."->modPart: part=$part, new part=$part_new");
		_d_chkspec($part,'part',get_class($this).'->modPart');
		_d_chkspec($part_new,'new part',get_class($this).'->modPart');
		_d_chkexist($this->Parts[$part],'part',get_class($this).'->modPart');
		_d_chknotexist($this->Parts[$part_new],'new part',get_class($this).'->modPart');
		$this->Parts[$part_new] = &$this->Parts[$part];
		//don't delete sub-structure
		unset($this->Parts[$part]);
		$this->getPart($part_new)->setName($part_new);
	}
/**
 * delPart - deletes part for this type
 * @return nothing
 * @param string $part - name of part
 */
	public function delPart($part = NULL) {
		_d_dbg(get_class($this)."->delPart: part=$part");
		_d_chkspec($part,'part',get_class($this).'->delPart');
		_d_chkexist($this->Parts[$part],'part',get_class($this).'->delPart');
		$this->Parts[$part]->delAllColumns();
		unset($this->Parts[$part]);
	}
/**
 * delAllParts - deletes all parts for this type
 * @return nothing
 */
	public function delAllParts() {
		_d_dbg(get_class($this)."->delAllParts");
		if(!empty($this->Parts)) {
			foreach($this->Parts as $partname => $part) {
				$this->delPart($partname);
			}
		}
	}
/**
 * getPart - return part by given name if exists
 * @return object ClassificationStructurePart - part by given name if exists
 * @param string $part - name of part
 */
	public function getPart($part) {
		_d_dbg(get_class($this)."->getPart: part=$part");
		_d_chkspec($part, 'part', get_class($this).'->getPart');
		//_d_chkexist($this->Parts[$part], 'part', get_class($this).'->getPart');
		return $this->Parts[$part];
	}
/**
 * getPartNames - return array of part names
 * @return array of part names
 */
	public function getPartNames() {
		return empty($this->Parts) ? array() : array_keys($this->Parts);
	}
/**
 * setName - magic method for internal hack of object property
 * @return nothing
 * @param object $name
 */
	public function setName($name) {
		_d_dbg(get_class($this)."->setName: name=$name");
		$this->Name = $name;
	}
/**
 * getName - magic method for steal private object property and blab it out
 * @return string - name of type
 */
	public function getName() {
		return $this->Name;
	}
}
/**
 * ClassificationStructurePart - class representing classification structure part, holds columns
 */
class ClassificationStructurePart {
/**
 * @var Name - name of part
 */
	private $Name;
	private $Caption;
/**
 * @var Columns - array of ClassificationStructureColumns objects, represents available columns for this part
 */
	private $Columns;
/**
 * __construct
 * @return nothing
 * @param string $part - name of part
 */
	public function __construct($part = NULL, $caption = NULL) {
		_d_dbg(get_class($this)."->__construct: part=$part");
		_d_chkspec($part,'part',get_class($this).'->__construct');
		$this->Name = $part;
		if(empty($caption)) {
			$caption = $part;
		}
		$this->Caption = $caption;
	}

/**
 * setCaption
 * @return
 */
	public function setCaption($caption) {
		$this->Caption = $caption;
	}
/**
 * getCaption
 * @return
 */
	public function getCaption() {
		return $this->Caption;
	}

	public function __toString() {
		return _D_BEGIN_MSG.print_r($this,true)._D_END_MSG;
	}
/**
 * addColumn - adds new column for this part
 * @return nothing
 * @param array $colspec - array holding column specification
 * colspec:
 * [idx] - index of column in classification
 * [name] - name of column
 * [caption] - human readable caption of column
 * [type] - data type of column
 * [default] - default value of column
 */
	public function addColumn($colspec = NULL) {
		_d_dbg(get_class($this)."->addColumn: colspec=".print_r($colspec,true));
		_d_chkspec($colspec['name'],'name',get_class($this).'->addColumn');
		_d_chknotexist($this->Columns[$colspec['name']],'name',get_class($this).'->addColumn');
		$this->Columns[$colspec['name']] = new ClassificationStructureColumn($colspec);
	}
/**
 * modColumn - modifies column properties
 * @return nothing
 * @param string $colname - name of column
 * @param array $colspec - array holding column specification
 * colspec:
 * [idx] - index of column in classification
 * [name] - name of column
 * [caption] - human readable caption of column
 * [type] - data type of column
 * [default] - default value of column
 */
	public function modColumn($colname = NULL,$colspec = NULL) {
		_d_dbg(get_class($this)."->modColumn: colname=$colname, colspec=".print_r($colspec,true));
		_d_chkspec($colname,'colname',get_class($this).'->modColumn');
		_d_chkexist($this->Columns[$colname],'colname',get_class($this).'->modColumn');
		if($colname == $colspec['name'] || empty($colspec['name'])) {
			// modify column
			$this->Columns[$colname]->setColumn($colspec);
		} else {
			// rename & modify column
			_d_chknotexist($this->Columns[$colspec['name']],'new colname',get_class($this).'->modColumn');
			$this->Columns[$colspec['name']] = &$this->Columns[$colname];
			unset($this->Columns[$colname]);
			$this->Columns[$colspec['name']]->setColumn($colspec);
		}
	}
/**
 * delColumn - deletes column for this part
 * @return nothing
 * @param string $colname - name of column
 */
	public function delColumn($colname = NULL) {
		_d_dbg(get_class($this)."->delColumn: colname=$colname");
		_d_chkspec($colname,'colname',get_class($this).'->delColumn');
		_d_chkexist($this->Columns[$colname],'colname',get_class($this).'->delColumn');
		unset($this->Columns[$colname]);
	}
/**
 * delAllColumns - deletes all columns for this part
 * @return nothing
 */
	public function delAllColumns() {
		_d_dbg(get_class($this)."->delAllColumns");
		unset($this->Columns);
	}
/**
 * getColumn - return column by given name if exists
 * @return object ClassificationStructureColumn - column by given name if exists
 * @param string $colname - name of column
 */
	public function getColumn($colname) {
		_d_dbg(get_class($this)."->getColumn: colname=$colname");
		_d_chkspec($colname, 'colname', get_class($this).'->getColumn');
		//_d_chkexist($this->Columns[$colname], 'colname', get_class($this).'->getColumn');
		return $this->Columns[$colname];
	}
/**
 * getAllColumns - return all columns as an array
 * @return array - all columns as an array
 * @param boolean $idx_prefix[optional] - add column index prefix to array items
 */
	public function getAllColumns($idx_prefix = false) {
		_d_dbg(get_class($this)."->getAllColumns");
		$r = array();
		if(!empty($this->Columns)) {
			foreach($this->Columns as $colname => $column) {
				//first getColumn returns object, while second converts it to array
				$a = $column->getColumnArray();
				$r[($idx_prefix ? $a['idx'].'_' : '').$colname] = $a;
			}
		}
		if(empty($r)) {
			$r = NULL;
		}
		return $r;
	}
/**
 * setName - magic method for internal hack of object property
 * @return nothing
 * @param object $name
 */
	public function setName($name) {
		_d_dbg(get_class($this)."->setName: name=$name");
		$this->Name = $name;
	}
/**
 * getName - magic method for steal private object property and blab it out
 * @return string - name of part
 */
	public function getName() {
		return $this->Name;
	}
}
/**
 * ClassificationStrcutureColumn - class representing classification structure column specification
 */
class ClassificationStructureColumn {
/**
 * @var Idx - index of column in classification
 */
	private $Idx;
/**
 * @var Name - name of column
 */
	private $Name;
/**
 * @var Caption - human readable caption of column
 */
	private $Caption;
/**
 * @var Type - data type of column
 */
	private $Type;
/**
 * @var Default - default value of column
 */
	private $Default;
/**
 * __construct
 * @return nothing
 * @param array $colspec - array holding column specification
 * colspec:
 * [idx] - index of column in classification
 * [name] - name of column
 * [caption] - human readable caption of column
 * [type] - data type of column
 * [default] - default value of column
 */
	public function __construct($colspec = NULL) {
		_d_dbg(get_class($this)."->__construct: colspec=".print_r($colspec,true));
		_d_chkspec($colspec['name'],'name',get_class($this).'->__construct');
		_d_chkspec($colspec['type'],'type',get_class($this).'->__construct');
		//idx: if not specified, is set to 0
		if(!empty($colspec['idx'])) {
			$this->Idx = $colspec['idx'];
		} else {
			$this->Idx = 0;
		}
		//name: must be specified
		$this->Name = $colspec['name'];
		//caption: if not specified is set to name
		if(!empty($colspec['caption'])) {
			$this->Caption = $colspec['caption'];
		} else {
			$this->Caption = $colspec['name'];
		}
		//type: must be specified
		$this->Type = $colspec['type'];
		//default: if not specified, remains unspecified
		if(!empty($colspec['default'])) {
			$this->Default = $colspec['default'];
		}
	}
/**
 * setColumn - sets new column properties, but specified only
 * @return nothing
 * @param array $colspec - array holding column specification
 * colspec:
 * [idx] - index of column in classification
 * [name] - name of column
 * [caption] - human readable caption of column
 * [type] - data type of column
 * [default] - default value of column
 */
	public function setColumn($colspec = NULL) {
		_d_dbg(get_class($this)."->setColumn: colspec=".print_r($colspec,true));
		_d_chkspec($colspec, 'colspec', get_class($this).'->setColumn');
		if(isset($colspec['idx'])) {
			$this->Idx = $colspec['idx'];
		}
		if(isset($colspec['name'])) {
			_d_chkspec($colspec['name'], 'colspec[name]', get_class($this).'->setColumn');
			$this->Name = $colspec['name'];
		}
		if(isset($colspec['caption'])) {
			$this->Caption = $colspec['caption'];
		}
		if(isset($colspec['type'])) {
			$this->Type = $colspec['type'];
		}
		if(isset($colspec['default'])) {
			$this->Default = $colspec['default'];
		}
	}
/**
 * getColumnArray - gets column properties as array
 * @return array colspec - array holding column specification
 * colspec:
 * [idx] - index of column in classification
 * [name] - name of column
 * [caption] - human readable caption of column
 * [type] - data type of column
 * [default] - default value of column
 */
	public function getColumnArray($colname = NULL) {
		_d_dbg(get_class($this)."->getColumn");
		return array(
			'idx' => $this->Idx,
			'name' => $this->Name,
			'caption' => $this->Caption,
			'type' => $this->Type,
			'default' => $this->Default,
		);
	}
	public function __toString() {
		return _D_BEGIN_MSG.print_r($this,true)._D_END_MSG;
	}
}
?>