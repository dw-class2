<?php
/**
 * Helper functions for debug and exceptions
 */
define('_D_DEBUG',true);
define('_D_DEBUG_LEVEL',10);
/*
 * _D_FDEBUG_LEVEL: lower value -> show less critical messages (= more messages)
 * _d_fdbg parameter: higher value -> more critical message
 */
define('_D_FDEBUG_LEVEL',10);
define('_D_BEGIN_MSG','<pre>');
define('_D_END_MSG','</pre>');
define('_D_BEGIN_DBG','<span style="background: silver;">');
define('_D_END_DBG','</span>');

function _d_fdbg($msg,$level = 0) {
	if($level >= _D_FDEBUG_LEVEL) {
		$f = fopen(DOKU_CONF.'/debug.txt','a');
		fprintf($f,"%s: %s\n",date('H:i:s'),print_r($msg,true));
		//msg(print_r($msg,true));
		fflush($f);
		fclose($f);
	}
}

function _d_dbg($msg,$level = 0) {
	if(_D_DEBUG && $level >= _D_DEBUG_LEVEL) {
		echo _D_BEGIN_MSG._D_BEGIN_DBG.print_r($msg,true)._D_END_DBG._D_END_MSG;
	}
}
/**
 * _d_chkspec - checks if variable is specified, throws exception if not
 * @return nothing
 * @param $var - variable
 * @param string $what - name of variable
 * @param string $where - scope (class and method)
 */
function _d_chkspec($var,$what,$where) {
	if(empty($var)) {
		throw new _D_NotSpecException(0, $where . ': ' . $what . ' not specified');
	}
}
/**
 * _d_chkexist - checks if variable (or item of structure) exists, throws exception if not
 * @return nothing
 * @param $var - variable
 * @param string $what - name of variable
 * @param string $where - scope (class and method)
 */
function _d_chkexist($var,$what,$where) {
	if(empty($var)) {
		throw new _D_NotExistsException(0, $where . ': ' . $what . ' not exists');
	}
}
/**
 * _d_chkexist - checks if variable (or item of structure) not yet exists, throws exception if already does
 * @return nothing
 * @param $var - variable
 * @param string $what - name of variable
 * @param string $where - scope (class and method)
 */
function _d_chknotexist($var,$what,$where) {
	if(!empty($var)) {
		throw new _D_ExistsException(0, $where . ': ' . $what . ' already exists');
	}
}

class _D_NotSpecException extends Exception {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($code = 0, $message = '_D_NotSpecException: required parameter not specified') {
		parent::__construct($message, $code);
	}
}

class _D_NotExistsException extends Exception {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($code = 0, $message = '_D_NotExistsException: required item not exists') {
		parent::__construct($message, $code);
	}
}

class _D_ExistsException extends Exception {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($code = 0, $message = '_D_ExistsException: item already exists') {
		parent::__construct($message, $code);
	}
}

class _D_FileIOException extends Exception {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($code = 0, $message = '_D_FileIOException: File operation failed') {
		parent::__construct($message, $code);
	}
}
?>