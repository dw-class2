<?php
/***********************************************/
$old_filename = '../../trash/data/classification-structure.conf';
/***********************************************/
use Facade\Courses,
	Facade\Parts,
	Facade\Columns,
	Entities\Part,
	Entities\Column,
	Tools\Debug,
	Tools\Globals;
// init simple class loader
//require_once(realpath(__DIR__.'/../..').'/loader.php');
require_once(realpath(__DIR__.'/../..').'/bootstrap.php');
// init old stuff
require('old/_D_.php');
require('old/ClassificationStructure.php');
require('old/ClassificationStructureFileFactory.php');

echo '<pre>';
echo "convert old classification structure serialized file to DB:\n";
echo "loading old: $old_filename\n";
if(!($old = ClassificationStructureFileFactory::load($old_filename))) {
	echo "!! failed - try check filename, path and/or permission\n";
	exit;
}

$em = Globals::getEntityManager();

$old_lang_names = $old->getLangNames();
foreach($old_lang_names as $old_lang_name) {
	echo "$old_lang_name\n";
	$old_lang = $old->getLang($old_lang_name);
	$old_type_names = $old_lang->getTypeNames();
	
	foreach($old_type_names as $old_type_name) {
		echo "\t$old_type_name: ";
		$old_type = $old_lang->getType($old_type_name);
		$old_part_names = $old_type->getPartNames();
		$new_course = Courses::getCourseByLangType($old_lang_name,$old_type_name);
		if(!$new_course) {
			echo "!! cannot find course by lang $old_lang_name and type $old_type_name\n";
			continue;
		} else {
			echo $new_course->code()."\n";
		}
		foreach($old_part_names as $old_part_name) {
			echo "\t\t$old_part_name: ";
			$old_part = $old_type->getPart($old_part_name);
			$old_columns = $old_part->getAllColumns();
			// find part or create it
			$new_part = Parts::getPartByCourseAndName($new_course->code(),$old_part_name);
			if(get_class($new_part) == 'Entities\Part') {
				echo "part found\n";
			} else {
				echo "creating new part: $old_part_name\n";
				$new_part = new Part;
				$new_part->name($old_part_name);
				$new_part->course($new_course);
				$em->persist($new_part);
			}
			foreach($old_columns as $old_column) {
				echo "\t\t\t$old_column[name] ($old_column[type]): ";
				$new_column = Columns::getColumnByCoursePartAndVariable($new_course->code(),$old_part_name,$old_column['name']);
				if(!empty($new_column)) {
					echo "loaded\n";
				} else {
					echo "new\n";
					$new_column = new Column;
				}
				//relation
				$new_column->part($new_part);
				//properties
				$new_column->idx($old_column['idx']);
				$new_column->variable($old_column['name']);
				$new_column->caption($old_column['caption']);
				$new_column->type($old_column['type']);
				if($old_column['type'] == 'expression') {
					$new_column->expression($old_column['default']);
				} else {
					$new_column->defaultValue($old_column['default']);
				}
				$em->persist($new_column);
			}
		}
  	}
}
$em->flush();
echo "done\n";
echo '</pre>';
?>
