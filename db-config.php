<?php

// Database connection information
$connectionOptions = array(
    'driver' => 'pdo_mysql',
	'user' => 'root',
	'password' => '',
	'host' => 'localhost',
	'port' => '3306',
	'dbname' => 'edux',
	//'unix_socket' => '/var/run/mysqld/mysqld.sock',
	'unix_socket' => '/tmp/mysql.sock',
);

?>
