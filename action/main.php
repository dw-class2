<?php
/**
 * Edit classification results
 */

use Tools\Globals,
	Tools\Debug,
	Tools\SyntaxHandler,
	Tools\DwHelper,
	Presenters\HtmlTable;

require_once(dirname(__FILE__).'/../bootstrap.php');

if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'action.php');

class action_plugin_class2_main extends DokuWiki_Action_Plugin {

	public function __construct() {
		Debug::setDebugTarget(Debug::DEBUG_OFF);
	}

/**
 * overridden translation method
 * @return translation or code
 */
	public function getLang($code) {
		$translation = trim(parent::getLang($code));
		if(empty($translation))
			return $code;
		return $translation;
	}
/**
 * Returns basic information about the plug-in required by Dokuwiki
 * @return array with basic information
 */
	function getInfo() {
		return array(
			'author'=> 'vojta jirkovsky',
			'email' => 'jirkovoj@fit.cvut.cz',
			'date' => '2010-2011',
			'name' => 'classification 2 - action',
			'desc' => 'classification 2 score action plugin (score editing, saving)',
			'url' => 'http://edux.fit.cvut.cz/',
		);
	}
/**
 * Register its handlers with the DokuWiki's event controller
 */
	public function register(&$controller) {
		//HTML_EDITFORM_OUTPUT has in data Doku_Form, is not preventable (but form is overwritable)
		//$controller->register_hook('HTML_EDITFORM_OUTPUT', 'BEFORE',  $this, 'editPokusHook');
		//TPL_ACT_RENDER has in data "action", we interest in edit
		$controller->register_hook('TPL_ACT_RENDER', 'BEFORE',  $this, 'editResultsHook');
		$controller->register_hook('TPL_ACT_RENDER', 'AFTER',  $this, 'editResultsHookFooter');
		//$controller->register_hook('HTML_EDITFORM_OUTPUT', 'BEFORE',  $this, 'editResultsHook');
		//$controller->register_hook('IO_WIKIPAGE_WRITE', 'BEFORE',  $this, 'saveResultsHook');
		$controller->register_hook('DOKUWIKI_STARTED', 'BEFORE',  $this, 'saveResultsHook');
	}

	public function editResultsHook(&$event, $param) {
		if($event->data == 'edit') {
			$content = DwHelper::getDwContent();
			preg_match_all('/^~~classification2:[^\n]*~~$/m',$content,&$matches);

			foreach($matches[0] as $match) {
				Debug::debug($match);
				$syntaxH = new SyntaxHandler($match);
				$syntaxH->handle();
				echo HtmlTable::generateStudentList($syntaxH,array('editable'=>TRUE));
			}
			//---zobrazit editable Table--
			echo '<!--editResultsHook--><div class="tabDwTextArea">';
		}
	}

	public function editResultsHookFooter(&$event, $param) {
		if($event->data == 'edit') {
			echo '</div><!--editResultsHookFooter-->';
		}
	}

	public function saveResultsHook(&$event, $param) {
		/// if results are POSTed -> save it
		if(isset($_POST['results'])) {
			//check csrf item
			if(!isset($_POST['sectok']) || !DwHelper::checkCSRF($_POST['sectok'])) {
				msg('Data saving failed due to CSRF.',-1);
				return;
			}

			$em = Globals::getEntityManager();
			$results = $_POST['results'];
			/// @todo get teacher object from logged user 
			$teacher = $em->find('Entities\Teacher','barinkl');
			if(Facade\Results::saveResults($results,$teacher)) {
				msg('Data was successfully saved.',1);
			} else {
				msg('Data saving failed.',-1);
			}
		}
	}
}
?>