<?php
// libraries
require_once 'simpletest/unit_tester.php';
require_once 'simpletest/mock_objects.php';
require_once 'simpletest/collector.php';
require_once 'simpletest/default_reporter.php';
require_once 'EduxHtmlReporter.php';

// test suites
class ClassificationTestSuite extends TestSuite {
		function __construct() {
				$this->TestSuite('classification');
				$this->addFile('tests/dijkstraTest.php');
				$this->addFile('tests/facadeTest.php');
		}
}

$test = new ClassificationTestSuite;
$test->run(new EduxHtmlReporter());
?>
