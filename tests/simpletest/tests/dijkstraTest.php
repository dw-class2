<?php
/**
 * dijkstraTest- unit test impl. for dijkrsta shunting yard & expression evaluation routines
 */

use Tools\Expression,
	Tools\Debug;
require_once(realpath(__DIR__.'/../../..').'/bootstrap.php');

class dijkstraTestCase extends UnitTestCase {

	function shuntingYardToString($expr) {
		$rpn = Expression::infixToPostfix($expr);
		foreach($rpn as $item) {
			$r .= $item['data'].' ';
		}
		return trim($r," \t\n");
	}

	function testInfixToPostfix() {
		$testSet = array(
		'1+1' => '1 1 +',
		'round(~11,493;1)' => '11.493 ~ 1 round',
		'round(~11,518;1)' => '11.518 ~ 1 round',
		'max(1+3;4-2)' => '1 3 + 4 2 - max',
		"10 <= 20 ? 'deset' : 'dvacet'" => '10 20 <= deset ? dvacet :',
		'empty(0)' => '0 empty',
		'empty(1 == 0)' => '1 0 == empty',
		);

		foreach($testSet as $expr => $correct) {
			$result = $this->shuntingYardToString($expr);
			$this->assertTrue($result == $correct, "Expression '$expr' doesn't convert into '$correct', but '$result'");
		}
	}

	function testEvaluation() {
		$testSet = array(
			'1+1' => '2',
			'2*3+1' => '7',
			'1+2*3' => '7',
			'(12/3)+(3*4)' => '16',
			'4*9/12' => '3',
			'12/3*6' => '24',
			'3^2' => '9',
			'2^3' => '8',

			'!0>0 && 6>5' => '1',
			'(10 >= 30 && 25 >= 20 ? 10 + 6 : 0)' => '0',
			'(40 >= 30 && 25 >= 20 ? 10 + 6 : 0)' => '16',
			'(10 >= 30 && 25 >= 20) ? 10 + 6 : 0' => '0',
			'(40 >= 30 && 25 >= 20) ? 10 + 6 : 0' => '16',
			'10 >= 30 && 25 >= 20 ? 10 + 6 : 0' => '0',
			'40 >= 30 && 25 >= 20 ? 10 + 6 : 0' => '16',
			"10 >= 30\r && 25\n >= 20 ?\n 10 + 6 : 0" => '0',
			"40 >= 30\t && 25\r >=\n 20 ?\t 10 + 6\r : 0" => '16',
			"10 <= 20 ? 'deset' : 'dvacet'" => 'deset',

			'1 + max(2; 5) - min (3 ;9) > 5 ? max ( 1 ; 10) : min ( 5 ; 3)' => '3',
			'10 + max(2; 5) - min (3 ;9) > 5 ? max ( 1 ; 10) : min ( 5 ; 3)' => '10',

			'floor(3.999)' => '3',
			'floor(~3.999)' => '-4',
			'ceil(3.999)' => '4',
			'ceil(~3.999)' => '-3',

			'round(11.49;0)' => '11',
			'round(11.5;0)' => '12',
			'round(11.51;0)' => '12',
			'round(~11.49;0)' => '-11',
			'round(~11.5;0)' => '-12',
			'round(~11.51;0)' => '-12',

			'round(11.493;1)' => '11.5',
			'round(11.518;1)' => '11.5',
			'round(~11.493;1)' => '-11.5',
			'round(~11.518;1)' => '-11.5',

			'mark(78;200)' => 'F',
			'mark(100;200)' => 'E',
			'mark(119;200)' => 'E',
			'mark(129;200)' => 'D',
			'mark(150;200)' => 'C',
			'mark(179;200)' => 'B',
			'mark(181;200)' => 'A',

			'empty(0)' => '0',
			'empty(1 == 0)' => '0',
			"empty('')" => '1',
			'empty("")' => '1',
			'empty(1 == 1)' => '0',
			);

		foreach($testSet as $expr => $correct) {
			$result = Expression::evaluate($expr);
			$this->assertTrue($result == $correct, "Expression '$expr' doesn't result into '$correct', but '$result'");
		}
	}
}
?>