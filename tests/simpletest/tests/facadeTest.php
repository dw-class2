<?php
/**
 * facadeTest - unit test impl. for Facade module
 */
use Facade\Students,
	Facade\Courses,
	Facade\Lessons,
	Facade\Results,
	Facade\Teams,
	Facade\Actions,
	Tools\Debug,
	Exception\NotExistsException;
require_once(realpath(__DIR__.'/../../..').'/bootstrap.php');

class ClassificationFacadeTestCase extends UnitTestCase {

	private $_stud = array();
	private $_cour = array();
	private $_team = array();
	private $_act = array();
	private $_exam = array();

	private $map = array(
		'Students::getStudentByLogin' => array(
			'return' => array(
				true => 'Entities\Student',
				false => 'NULL'
			),
		),
		'Students::getStudentsByLogins' => array(
			'return' => array(
				true => 'array of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByCourseAndLesson' => array(
			'return' => array(
				true => 'Doctrine\ORM\PersistentCollection of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByLesson' => array(
			'return' => array(
				true => 'array of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByCoursesAndLesson' => array(
			'return' => array(
				true => 'array of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByLessonTypeAndNumber' => array(
			'return' => array(
				true => 'array of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByLecture' => array(
			'return' => array(
				true => 'array of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByTutorial' => array(
			'return' => array(
				true => 'array of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByLaboratory' => array(
			'return' => array(
				true => 'array of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByTeam' => array(
			'return' => array(
				true => 'Doctrine\ORM\PersistentCollection of Entities\Student',
				false => 'empty array'
			),
		),
		'Students::getStudentsByActionOrigId' => array(
			'return' => array(
				true => 'Doctrine\ORM\PersistentCollection of Entities\Student',
				false => 'empty array'
			),
		),
		'Actions::getAllActions' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getAllSingleshotActions' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getAllExams' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getActionById' => array(
			'return' => array(
				true => 'Entities\Action',
				false => 'NULL'
			),
		),
		'Actions::getActionByOrigId' => array(
			'return' => array(
				true => 'Entities\Action',
				false => 'NULL'
			),
		),
		'Actions::getActionsByType' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getActionsByStudent' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getActionsByTypeAndStudent' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getSingleshotActionsByStudent' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getExamsByStudent' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Courses::getAllCourses' => array(
			'return' => array(
				true => 'array of Entities\Course',
				false => 'empty array'
			),
		),
		'Courses::getCourseByCode' => array(
			'return' => array(
				true => 'Entities\Course',
				false => 'NULL'
			),
		),
		'Courses::getCoursesByCodes' => array(
			'return' => array(
				true => 'array of Entities\Course',
				false => 'empty array'
			),
		),
		'Courses::getCourseByLangType' => array(
			'return' => array(
				true => 'Entities\Course',
				false => 'NULL'
			),
		),
		'Courses::getCourseByStudent' => array(
			'return' => array(
				true => 'Entities\Course',
				false => 'NULL'
			),
		),
		'Lessons::getLessonsByCourseAndType' => array(
			'return' => array(
				true => 'array of Entities\Lesson',
				false => 'empty array'
			),
		),
		'Lessons::getLessonsByCourse' => array(
			'return' => array(
				true => 'array of Entities\Lesson',
				false => 'empty array'
			),
		),
		'Lessons::getLessonsByType' => array(
			'return' => array(
				true => 'array of Entities\Lesson',
				false => 'empty array'
			),
		),
		'Lessons::getLessonByCourseAndNumber' => array(
			'return' => array(
				true => 'Entities\Lesson',
				false => 'NULL'
			),
		),
		'Lessons::getLessonsByCoursesAndNumber' => array(
			'return' => array(
				true => 'array of Entities\Lesson',
				false => 'empty array'
			),
		),
		'Results::getResult' => array(
			'return' => array(
				true => 'Entities\Result',
				false => 'NULL'
			),
		),
		'Teams::getAllTeams' => array(
			'return' => array(
				true => 'array of Entities\Team',
				false => 'empty array'
			),
		),
		'Teams::getTeamByName' => array(
			'return' => array(
				true => 'Entities\Team',
				false => 'NULL'
			),
		),
		'Teams::getTeamsByStudent' => array(
			'return' => array(
				true => 'array of Entities\Team',
				false => 'empty array'
			),
		),
		'Actions::getAllActions' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getAllSingleshotActions' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getAllExams' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getActionByOrigId' => array(
			'return' => array(
				true => 'Entities\Action',
				false => 'NULL'
			),
		),
		'Actions::getActionsByType' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getActionsByStudent' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getActionsByTypeAndStudent' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getSingleshotActionsByStudent' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
		'Actions::getExamsByStudent' => array(
			'return' => array(
				true => 'array of Entities\Action',
				false => 'empty array'
			),
		),
	);

	private function checkReturnDataType($var,$method,$line) {
		$t = &$this->map[$method]['return'][true];
		$tp = preg_replace('/Entities(.)([\w]*)/','Proxies$1Entities$2Proxy',$t);
		$f = &$this->map[$method]['return'][false];
		$d = Debug::datatype($var);
		$r = ($d == $t) || ($d == $f) || ($d == $tp);
		
		$echo = '<pre>'.$line.': '.$method.' returns '.$d;
		if(!$r) $echo .= "\n***FAILS***";
		if($d == $f) $echo .= "\n***FALSE***";
		if($d == $tp) $echo .= "\n***PROXY***";
		$echo .= '</pre>';
		// print message only if FAILS, FALSE or PROXY
		if(!$r || $d == $f || $d == $tp) echo $echo;
		
		$this->assertTrue($r,'Line '.$line.': method '.$method.' returns '.$d.', expected either '.$t.' or '.$f);
		return $r;
	}

	private function getSamples() {
		try {
			if(!$this->_stud) {
				$students = Students::getStudentsByLecture('1');
				$this->checkReturnDataType($students,'Students::getStudentsByLecture',__LINE__);
				$i = 0;
				foreach($students as $student) {
					$this->_stud[$i] = $student->login();
					if($i>=3) break;
					$i++;
				}
			Debug::debug($this->_stud);
			}
			if(!$this->_cour) {
				$courses = Courses::getAllCourses();
				$this->checkReturnDataType($courses,'Courses::getAllCourses',__LINE__);
				$i = 0;
				foreach($courses as $course) {
					$this->_cour[$i] = $course->code();
					if($i>=3) break;
					$i++;
				}
			Debug::debug($this->_cour);
			}
			if(!$this->_team) {
				$teams = Teams::getAllTeams();
				$this->checkReturnDataType($teams,'Teams::getAllTeams',__LINE__);
				$i = 0;
				foreach($teams as $team) {
					$this->_team[$i] = $team->name();
					if($i>=3) break;
					$i++;
				}
			Debug::debug($this->_team);
			}
			if(!$this->_act) {
				$actions = Actions::getAllSingleshotActions();
				$this->checkReturnDataType($actions,'Actions::getAllSingleshotActions',__LINE__);
				$i = 0;
				foreach($actions as $action) {
					$this->_act[$i]['orig_id'][] = $action->origId();
					$j = 0;
					foreach($action->students() as $student) {
						$this->_act[$i]['students'][] = $student->login();
						if($j>=3) break;
						$j++;
					}
					if($i>=3) break;
					$i++;
				}
			Debug::debug($this->_act);
			}
			if(!$this->_exam) {
				$exams = Actions::getAllExams();
				$this->checkReturnDataType($actions,'Actions::getAllExams',__LINE__);
				$i = 0;
				foreach($exams as $exam) {
					$this->_exam[$i]['orig_id'][] = $exam->origId();
					$j = 0;
					foreach($exam->students() as $student) {
						$this->_exam[$i]['students'][] = $student->login();
						if($j>=3) break;
						$j++;
					}
					if($i>=3) break;
					$i++;
				}
			Debug::debug($this->_exam);
			}
		} catch(Exception $e) {
			echo 'Exception: '.$e->getMessage();
		}
	}

	public function testStudents() {
		$this->getSamples();

		$students = Students::getStudentByLogin($this->_stud[0]);
		$this->checkReturnDataType($students,'Students::getStudentByLogin',__LINE__);

		$students = Students::getStudentsByLogins($this->_stud);
		$this->checkReturnDataType($students,'Students::getStudentsByLogins',__LINE__);

		$students = Students::getStudentsByCourseAndLesson($this->_cour[0],'101');
		$this->checkReturnDataType($students,'Students::getStudentsByCourseAndLesson',__LINE__);

		$students = Students::getStudentsByLesson('101');
		$this->checkReturnDataType($students,'Students::getStudentsByLesson',__LINE__);

		$students = Students::getStudentsByCoursesAndLesson($this->_cour,'102');
		$this->checkReturnDataType($students,'Students::getStudentsByCoursesAndLesson',__LINE__);

		$students = Students::getStudentsByLessonTypeAndNumber('tutorial','102');
		$this->checkReturnDataType($students,'Students::getStudentsByLessonTypeAndNumber',__LINE__);

		$students = Students::getStudentsByLecture('1');
		$this->checkReturnDataType($students,'Students::getStudentsByLecture',__LINE__);

		$students = Students::getStudentsByTutorial('102');
		$this->checkReturnDataType($students,'Students::getStudentsByTutorial',__LINE__);

		$students = Students::getStudentsByLaboratory('1102');
		$this->checkReturnDataType($students,'Students::getStudentsByLaboratory',__LINE__);

		$students = Students::getStudentsByTeam($this->_team[0]);
		$this->checkReturnDataType($students,'Students::getStudentsByTeam',__LINE__);
		
		$students = Students::getStudentsByActionOrigId($this->_act[0]['orig_id'][0]);
		$this->checkReturnDataType($students,'Students::getStudentsByActionOrigId',__LINE__);
	}

	public function testCourses() {
		$this->getSamples();

		$courses = Courses::getAllCourses();
		$this->checkReturnDataType($courses,'Courses::getAllCourses',__LINE__);

		$courses = Courses::getCourseByCode($this->_cour[0]);
		$this->checkReturnDataType($courses,'Courses::getCourseByCode',__LINE__);

		$courses = Courses::getCoursesByCodes($this->_cour);
		$this->checkReturnDataType($courses,'Courses::getCoursesByCodes',__LINE__);

		$courses = Courses::getCourseByLangType('cs','fulltime');
		$this->checkReturnDataType($courses,'Courses::getCourseByLangType',__LINE__);

		$courses = Courses::getCourseByStudent($this->_stud[0]);
		$this->checkReturnDataType($courses,'Courses::getCourseByStudent',__LINE__);
	}

	public function testLessons() {
		$this->getSamples();

		$lessons = Lessons::getLessonsByCourse($this->_cour[0]);
		$this->checkReturnDataType($lessons,'Lessons::getLessonsByCourse',__LINE__);

		$lessons = Lessons::getLessonByCourseAndNumber($this->_cour[0],'101');
		$this->checkReturnDataType($lessons,'Lessons::getLessonByCourseAndNumber',__LINE__);

		$lessons = Lessons::getLessonsByCoursesAndNumber($this->_cour,'101');
		$this->checkReturnDataType($lessons,'Lessons::getLessonsByCoursesAndNumber',__LINE__);

		$lessons = Lessons::getLessonsByCourseAndType($this->_cour[0],'P');
		$this->checkReturnDataType($lessons,'Lessons::getLessonsByCourseAndType',__LINE__);

		$lessons = Lessons::getLessonsByCourseAndType(NULL,'P');
		$this->checkReturnDataType($lessons,'Lessons::getLessonsByCourseAndType',__LINE__);

		$lessons = Lessons::getLessonsByType('P');
		$this->checkReturnDataType($lessons,'Lessons::getLessonsByType',__LINE__);
	}

	public function testResults() {
		///nahradit parametry necim dynamickym
		$results = Results::getResult($this->_stud[0],'tutorial','zapocet');
		$this->checkReturnDataType($results,'Results::getResult',__LINE__);
	}

	public function testTeams() {
		$teams = Teams::getAllTeams();
		$this->checkReturnDataType($teams,'Teams::getAllTeams',__LINE__);

		$teams = Teams::getTeamByName($this->_team[0]);
		$this->checkReturnDataType($teams,'Teams::getTeamByName',__LINE__);

		$teams = Teams::getTeamsByStudent($this->_stud[0]);
		$this->checkReturnDataType($teams,'Teams::getTeamsByStudent',__LINE__);
	}

	public function testActions() {
		$actions = Actions::getAllActions();
		$this->checkReturnDataType($actions,'Actions::getAllActions',__LINE__);

		$actions = Actions::getAllSingleshotActions();
		$this->checkReturnDataType($actions,'Actions::getAllSingleshotActions',__LINE__);

		$actions = Actions::getAllExams();
		$this->checkReturnDataType($actions,'Actions::getAllExams',__LINE__);

		$actions = Actions::getActionsByType('JA');
		$this->checkReturnDataType($actions,'Actions::getActionsByType',__LINE__);

		$actions = Actions::getActionsByStudent($this->_act[0]['students'][0]);
		$this->checkReturnDataType($actions,'Actions::getActionsByStudent',__LINE__);

		$actions = Actions::getActionsByTypeAndStudent('JA',$this->_act[0]['students'][0]);
		$this->checkReturnDataType($actions,'Actions::getActionsByTypeAndStudent',__LINE__);

		$actions = Actions::getSingleshotActionsByStudent($this->_act[0]['students'][0]);
		$this->checkReturnDataType($actions,'Actions::getSingleshotActionsByStudent',__LINE__);
	}
}
?>