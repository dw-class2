<?php

/**
 * Reporter class based on original HtmlReporter, however, 
 * output is slightly changed and more readable hopefully
 * 
 * PHP4 and PHP5 compatible.
 * 
 * @author Tomas Kadlec <tomas@tomaskadlec.net>
 * @version 2009-07-21
 */
class EduxHtmlReporter extends SimpleReporter {
	
	var $_character_set;
	var $id;
	// indicates that whole method passes
	var $pass;
	var $passCount;
	var $totalCount;

	/**
	 *    Does nothing yet. The first output will
	 *    be sent on the first test start. For use
	 *    by a web browser.
   *    @access public
   */
	function HtmlReporter($character_set = 'UTF-8') {
		$this->SimpleReporter();
		$this->_character_set = $character_set;
		$this->idgen = 0;
	}

	/**
   *    Paints the top of the web page setting the
	 *    title to the name of the starting test.
	 *    @param string $test_name      Name class of test.
	 *    @access public
	 */
	function paintHeader($test_name) {
		$this->sendNoCacheHeaders();
		print "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";
		print "<html>\n<head>\n<title>$test_name</title>\n";
		print "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=" .
		      $this->_character_set . "\">\n".
          "<style type=\"text/css\">\n".
		      $this->_getCss() . "\n".
		      "</style>\n".
		      "<script type=\"text/javascript\">\n".
		      $this->_getScript().
		      "</script>\n".
		      "</head>\n<body>\n".
		      "<h1>$test_name</h1>\n";
		flush();
	}

	/**
	 *    Send the headers necessary to ensure the page is
   *    reloaded on every request. Otherwise you could be
   *    scratching your head over out of date test data.
   *    @access public
   *    @static
   */
	function sendNoCacheHeaders() {
		if (! headers_sent()) {
			header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
		}
	}
	
	/**
	*    Paints the end of the test with a summary of
	*    the passes and failures.
	*    @param string $test_name        Name class of test.
	*    @access public
	*/
	function paintFooter($test_name) {
		print '<div id="result">';
		print '<h1>Final results</h1>';
		$colour = ($this->getFailCount() + $this->getExceptionCount() > 0 ? "red" : "green");
		print "<div style=\"";
		print "padding: 8px; margin-top: 1em; background-color: $colour; color: white;";
		print "\">";
		print $this->getTestCaseProgress() . "/" . $this->getTestCaseCount();
		print " test cases complete:\n";
		print "<strong>" . $this->getPassCount() . "</strong> passes, ";
		print "<strong>" . $this->getFailCount() . "</strong> fails and ";
		print "<strong>" . $this->getExceptionCount() . "</strong> exceptions.";
		print "</div></div>\n";
		print '<div id="footer"><hr>Page generated: '.date(DATE_ISO8601).'</div>';
		print "</body>\n</html>\n";
	}
	
	
	/**
	 * Prints filename
	 * @param $test_name
	 * @param $size
	 */
	function paintGroupStart($test_name, $size) {  
		parent::paintGroupStart($test_name, $size);
		if (preg_match('/\.php$/', $test_name))
			print '<h2>'.$test_name.'</h2>';
	}
	/* function paintGroupEnd($test_name) {}*/
	
	/**
	 * Prints name of current case
	 * @param $test_name
	 */
	function paintCaseStart($test_name) {
		parent::paintCaseStart($test_name);
		print '<h3>CASE: '.$test_name.'</h3>';
		print '<table>';
	}
  
	/**
	 * Flush content
	 * @param $test_name
	 */
	function paintCaseEnd($test_name) {
		parent::paintCaseEnd($test_name);	
		print '</table>'; 
	}

	/**
	 * Opens table, prints name of current test method
	 * @param $test_name
	 */
  function paintMethodStart($test_name) {
  	parent::paintMethodStart($test_name);
  	$this->pass = true;
  	$this->passCount = 0;
  	$this->totalCount = 0;
		print '<tr class="method"><th colspan="3">'.$this->_htmlEntities($test_name).'</th></tr>';
  }
  
  /**
   * Closes table
   * @param $test_name
   */
	function paintMethodEnd($test_name) {
		parent::paintMethodEnd($test_name);
		// if whole method passes print it
		if ($this->pass == true)
			print '<tr class="line"><th class="higher pass" colspan="3">Passed ('.$this->passCount.'/'.$this->totalCount.')</th></tr>';
		else
			print '<tr class="line"><th class="higher fail" colspan="3">Failed ('.($this->totalCount - $this->passCount).'/'.$this->totalCount.')</th></tr>';
		// empty line - separator
		print '<tr class="empty"><td colspan="3">&nbsp;</td></tr>';
	}

	/**
	*    Paints the test failure with a breadcrumbs
	*    trail of the nesting test suites below the
	*    top level test.
	*    @param string $message    Failure message displayed in
	*                              the context of the other tests.
	*    @access public
	*/
	function paintFail($message) {
		parent::paintFail($message);
		$this->pass = false;
		$this->totalCount++;
		// parse message
		preg_match('/line[[:space:]]*([0-9]+)/', $message, $matches);
		$line = @$matches[1];
		preg_match('/^(.*)(at \[)/', $message, $matches);
		$msg = @$matches[1];
		// display result
		echo '<tr><th class="fail" rowspan="2">Failed</th><th>Line<td>'.$this->_htmlEntities($line).'</td></tr>'.
		     '<tr class="line"><th>Message<td>'.$this->_htmlEntities($msg).'</td></tr>';
	}
	
	/**
	 * Informs about successful passes
	 * @param $message
	 */
	function paintPass($message) {
		parent::paintPass($message);
		$this->passCount++;
		$this->totalCount++;
	}
	
	/**
	*    Paints a PHP error.
	*    @param string $message        Message is ignored.
	*    @access public
	*/
	function paintError($message) {
		parent::paintError($message);
		$this->pass = false;
		$this->totalCount++;
	
		// parse message
		preg_match('/line[[:space:]]*([0-9]+)/', $message, $matches);
		$line = @$matches[1];
		preg_match('/^(.*)(at \[)/', $message, $matches);
		$msg = @$matches[1];
		// display result
		echo '<tr><th class="fail" rowspan="2">Error</th><th>Line<td>'.$this->_htmlEntities($line).'</td></tr>'.
		     '<tr class="line"><th>Message<td>'.$this->_htmlEntities($msg).'</td></tr>';
	}
	
	/**
	*    Paints a PHP exception.
	*    @param Exception $exception        Exception to display.
	*    @access public
	*/
	function paintException($exception) {
		parent::paintException($exception);
		$this->pass = false;
		$this->totalCount++;
		
		// increment id
		$this->_id();
		// display result
		echo '<tr><th class="fail" rowspan="5">Exception</th><th>File<td>'.$this->_htmlEntities($exception->getFile()).'</td></tr>'.
		     '<tr><th>Line</th><td>'.$this->_htmlEntities($exception->getLine()).'</td></tr>'.
		     '<tr><th>Message</th><td>'.$this->_htmlEntities($exception->getMessage()).'</td></tr>'.
		     '<tr><th>Code</th><td>'.$this->_htmlEntities($exception->getCode()).'</td></tr>'.
		     '<tr class="line"><th>Trace</th><td>'.
					 '<input type="button" value="Show" onclick="javascript:show('.$this->id.')" >'.
		       '<pre id="'.$this->id.'" onclick="javascript:hide('.$this->id.')" class="hidden">'.
		       '//Click to close//'."\n\n".
		       $this->_htmlEntities($exception->getTraceAsString()).'</pre>'.
		     '</td></tr>';
	}
	
	/**
	 *    Prints the message for skipping tests.
	 *    @param string $message    Text of skip condition.
	 *    @access public
	 */
	function paintSkip($message) {
		parent::paintSkip($message);
		echo '<tr class="line"><th class="higher skip" colspan="3">Skipped'.(!empty($message) ? ' '.$message : '').'</th></tr>';
	}
	
	/**
	 *    Paints formatted text such as dumped variables.
	 *    @param string $message        Text to show.
	 *    @access public
	 */
	function paintFormattedMessage($message) {
		$this->_id();
		echo '<tr class="line"><th class="higher info">Dump</th><td></td><td>'.
					 '<input type="button" value="Show" onclick="javascript:show('.$this->id.')" >'.
		       '<pre id="'.$this->id.'" onclick="javascript:hide('.$this->id.')" class="hidden">'.
		       '//Click to close//'."\n\n".
		       $this->_htmlEntities($message).'</pre>'.
		     '</td></tr>';
	}
	
	/**
	 *    Character set adjusted entity conversion.
	 *    @param string $message    Plain text or Unicode message.
	 *    @return string            Browser readable message.
	 *    @access protected
	 */
	function _htmlEntities($message) {
		return htmlentities($message, ENT_COMPAT, $this->_character_set);
	}
	
	/**
	 *    Paints the CSS. Add additional styles here.
	 *    @return string            CSS code as text.
	 *    @access protected
	 */
	function _getCss() {
		return
		  "body { width: 90%; margin: 0 5%; }".
			"table { width: 100%; border-collapse: collapse; }".
		  "table tr th { width: 10%; text-align: left;}".  
			"table tr.line { border-bottom: 1px solid black; }".
			"table tr.method { text-align: left; font-size: 1.25em; border-bottom: 2px solid black ; }".
			"table tr.empty, table tr.empty td { border: 0; }".
			".fail, .pass, .skip, .info { font-size: 1.1em; }".
		  ".higher { height: 2em; }".
			".fail { color: red; }" .
			".pass { color: green; }" .
			".skip { color: lightgray; }" .
		  ".info { color: #6BA4FF; }".
			" pre { background-color: lightgray; color: inherit; }".
			".hidden { overflow: scroll; position: fixed; z-index: 20; display: none; top: 0; left: 0; width: 90%; margin: 2% 5%; height: 90%;}".
			"#result {position: absolute; z-index: 10; top: 0; right: 5%; background-color: white; }".
			"#result h1 {text-align: right;}".
			"#footer { font-size: 0.8em; padding-top: 2em;}"
		;
	}
	
	/**
	 * Paints Javascript
	 * @return string Javascript code as text.
	 */
	function _getScript() {
		return 
			'<!--'."\n".
			'function show(id) { document.getElementById(id).style.display = "block"; }'."\n".
			'function hide(id) { document.getElementById(id).style.display = "none"; }'."\n".
			'-->'."\n";
	}
	
	/**
	 * Increments idgen and return its value
	 * @return integer
	 */
	function _id() {
		$this->id++;
		return $this->id;
	}
}
?>