<?php
use Doctrine\Common\ClassLoader,
	Structure\SerializeStorage,
	Tools\Debug,
	Tools\Globals;
/// Bootstrap Doctrine for web
require_once('cli-config.php');
/// Sets entity manager to globals
Globals::setEntityManager($em);
?>