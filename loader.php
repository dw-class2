<?php
/*
* Simple class loader - when Doctrine Autoloader is not used
*/
function defaultAutoload($className) {
	
	$prefix = realpath(__DIR__);
	$paths = array(
		'/classes/',
		'/classes/Tools/',
		'/classes/Exception/',
		'/classes/Facade/',
		'/classes/Presenters/',
		'/classes/Entities/',
		'/classes/Proxies/',
		'/classes/Structure/',
		'/classes/UI/',
	);
	
	foreach($paths as $path) {	
		$filename = strtr($prefix.$path.$className.'.php',array('\\' => '/', '//' => '/'));

		if(file_exists($filename)) {
			require($filename);
			return;
		} 
	}
	throw new Exception('loader: Cannot load class '.$className);
}

spl_autoload_register(defaultAutoload);
?>
