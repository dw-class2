<?php
use Doctrine\Common\ClassLoader,
	Doctrine\ORM\Configuration,
	Doctrine\ORM\EntityManager,
	Doctrine\Common\Cache\ApcCache,
	Doctrine\Common\Cache\ArrayCache,
	Doctrine\DBAL\Event\Listeners\MysqlSessionInit;

require_once 'lib/Doctrine/Common/ClassLoader.php';

// Set up class loading. You could use different autoloaders, provided by your favorite framework,
// if you want to.
/*
$classLoader = new ClassLoader('Doctrine\ORM', realpath(__DIR__ . '/lib'));
$classLoader->register();
$classLoader = new ClassLoader('Doctrine\DBAL', realpath(__DIR__ . '/lib'));
$classLoader->register();
$classLoader = new ClassLoader('Doctrine\Common', realpath(__DIR__ . '/lib'));
$classLoader->register();
$classLoader = new ClassLoader('Symfony', realpath(__DIR__ . '/lib'));
$classLoader->register();
*/
$classLoader = new ClassLoader('Doctrine\ORM', 'lib');
$classLoader->register();
$classLoader = new ClassLoader('Doctrine\DBAL', 'lib');
$classLoader->register();
$classLoader = new ClassLoader('Doctrine\Common', 'lib');
$classLoader->register();
$classLoader = new ClassLoader('Symfony', 'lib');
$classLoader->register();

$namespaces = array(
	'Entities',
	'Proxies',
	// Custom class loaders
	'Structure',
	'Exception',
	'Tools',
	'Facade',
	'Presenters',
	'UI',
);

foreach($namespaces as $namespace) {
	$classLoader = new ClassLoader($namespace, realpath(__DIR__ . '/classes'));
	$classLoader->register();
}

// Set up caches
$config = new Configuration;
//$cache = new ApcCache;
$cache = new ArrayCache;
$config->setMetadataCacheImpl($cache);
$driverImpl = $config->newDefaultAnnotationDriver(array(__DIR__. '/classes/Entities'));
$config->setMetadataDriverImpl($driverImpl);
$config->setQueryCacheImpl($cache);

// Proxy configuration
$config->setProxyDir(__DIR__ . '/classes/Proxies');
$config->setProxyNamespace('Proxies');
$config->setAutoGenerateProxyClasses(false);

require('db-config.php');

// Create EntityManager
$em = EntityManager::create($connectionOptions, $config);
$em->getEventManager()->addEventSubscriber(new MysqlSessionInit('utf8', 'utf8_unicode_ci'));

// Helpers for console
$helperSet = new \Symfony\Component\Console\Helper\HelperSet;
$helperSet->set(new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()), 'db');
$helperSet->set(new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em), 'em');

?>
