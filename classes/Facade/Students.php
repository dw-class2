<?php
namespace Facade;
use Tools\Globals;
use Exception\GenericException;

class Students {
	/**
	* Get student by login
	* <code>$student = Students::getStudentByLogin('alexaluk');</code>
	* @param string login
	* @return Entities\Student or NULL
	*/
	public static function getStudentByLogin($login) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Student')->findOneBy(array('login' => $login));
	}
	/**
	* Get students by logins
	* <code>$students = Students::getStudentsByLogins(array('adameji4','aletibha','arletjir'));</code>
	* @param array of string logins
	* @return array of Entities\Student or empty array
	*/
	public static function getStudentsByLogins($logins) {
		$em = Globals::getEntityManager();
		if(!is_array($logins)) {
			throw new GenericException('getStudentsByLogins: argument is not an array');
		}
		if(empty($logins)) {
			return array();
		}
		$in = ''; $i = 1;
		foreach($logins as $login) {
			$in .= (($in) ? ',' : '')."?$i";
			$i++;
		}
		if($in != '') {
			$i = 1;
			$q = 'SELECT s FROM Entities\Student s WHERE s.login IN('.$in.')';
			$query = $em->createQuery($q);
			foreach($logins as $login) {
				$query->setParameter($i,$login);
				$i++;
			}
		}
		return $query->getResult();
	}
	/**
	* Get students by course code and lesson number
	* <code>$students = Students::getStudentsByCourseAndLesson('BI-UOS','101');</code>
	* @param string course code (e.g. BI-UOS)
	* @param int lesson number (e.g. 101)
	* @return Doctrine\ORM\PersistentCollection of Entities\Student or empty array
	*/
	public static function getStudentsByCourseAndLesson($course_code,$number) {
		$em = Globals::getEntityManager();
		$lesson = Lessons::getLessonByCourseAndNumber($course_code,$number);
		return $lesson ? $lesson->students() : array();
	}
	/**
	* Get students by lesson whatever type it is
	* <code>$students = Students::getStudentsByLesson('109');</code>
	* @param int lesson number
	* @return array of Entities\Student or empty array
	*/
	public static function getStudentsByLesson($number) {
		$em = Globals::getEntityManager();
		$all_students = array();
		$lessons = $em->getRepository('Entities\Lesson')->findBy(array('number' => $number));
		foreach($lessons as $lesson) {
			$students = $lesson->students();
			foreach($students as $student) {
				$all_students[] = $student;
			}
		}
		return $all_students;
	}
	/**
	* Get students by course codes and lesson numbers (in case of conjoined courses e.g. TW1 + WA1)
	* <code>$students = Students::getStudentsByCoursesAndLesson(array('A4B39WA1','A7B39WA1'),'102');</code>
	* @param array of string course codes
	* @param int lesson number
	* @return array of Entities\Student or empty array
	*/
	public static function getStudentsByCoursesAndLesson($course_codes,$number) {
		$em = Globals::getEntityManager();
		$all_students = array();
		$lessons = Lessons::getLessonsByCoursesAndNumber($course_codes,$number);
		foreach($lessons as $lesson) {
			$students = $lesson->students();
			foreach($students as $student) {
				$all_students[] = $student;
			}
		}
		return $all_students;
	}
	/**
	* Get students by lesson of specified type
	* <code>$students = Students::getStudentsByLessonTypeAndNumber('tutorial','102');</code>
	* @param string lesson type (i.e. lecture, laboratory, tutorial)
	* @param int lesson number
	* @return array of Entities\Student or empty array
	*/
	public static function getStudentsByLessonTypeAndNumber($lesson_type,$number) {
		$em = Globals::getEntityManager();
		if(!in_array($lesson_type,array('lecture','laboratory','tutorial'))) {
			throw new GenericException('getStudentsByLessontypeAndNumber: invalid type: '.$lesson_type);
		}
		$q = 'SELECT s FROM Entities\Student s LEFT JOIN s.'.$lesson_type.' l WHERE l.number=:number';
		$query = $em->createQuery($q);
		$query->setParameters(array('number' => $number));
		return $query->getResult();
	}
	/**
	* Get students by lesson of specified type lecture
	* <code>$students = Students::getStudentsByLecture('1');</code>
	* @param int lesson number
	* @return array of Entities\Student or empty array
	*/
	public static function getStudentsByLecture($number) {
		return Students::getStudentsByLessonTypeAndNumber('lecture',$number);
	}
	/**
	* Get students by lesson of specified type tutorial
	* <code>$students = Students::getStudentsByTutorial('102');</code>
	* @param int lesson number
	* @return array of Entities\Student or empty array
	*/
	public static function getStudentsByTutorial($number) {
		return Students::getStudentsByLessonTypeAndNumber('tutorial',$number);
	}
	/**
	* Get students by lesson of specified type laboratory
	* <code>$students = Students::getStudentsByLaboratory('1102');</code>
	* @param int lesson number
	* @return array of Entities\Student or empty array
	*/
	public static function getStudentsByLaboratory($number) {
		return Students::getStudentsByLessonTypeAndNumber('laboratory',$number);
	}
	/**
	* Gets students by team name
	* <code>$students = Students::getStudentsByTeam('Beta');</code>
	* @param string team name
	* @return Doctrine\ORM\PersistentCollection of Entities\Student or empty array
	*/
	public static function getStudentsByTeam($team_name) {
		$team = Teams::getTeamByName($team_name);
		if($team) {
			return $team->students();
		} else {
			return array();
		}
	}
	/**
	* Gets students by action type
	* <code>$students = Students::getStudentsByActionOrigId('263993314205');</code>
	* @param int action original id
	* @return Doctrine\ORM\PersistentCollection of Entities\Student or empty array
	*/
	public static function getStudentsByActionOrigId($action_orig_id) {
		$action = Actions::getActionByOrigId($action_orig_id);
		if($action) {
			return $action->students();
		} else {
			return array();
		}
	}
}
?>