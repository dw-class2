<?php
namespace Facade;
use Tools\Globals;

class Results {
	/** TODO! do it better **/
	/**
	* Get particular student result
	* <code>$result = Results::getResult('alexaluk','tutorial','zapocet');</code>
	* @param string student login
	* @param string classification structure part
	* @param string classification structure column (in selected part)
	* @return Entities\Result or NULL
	*/
	public static function getResult($login,$part,$column) {
		$em = Globals::getEntityManager();
		if($course = Courses::getCourseByStudent($login)) {
			$course = $course->code();
			$q = 'SELECT r FROM Entities\Result r LEFT JOIN r.part p WHERE r.student_login=:login AND p.course_code=:course AND p.name=:part AND r.column=:column';
			$query = $em->createQuery($q);
			$query->setParameters(array('login' => $login,'course' => $course,'part' => $part,'column' => $column));
			$query->setMaxResults(1);
			$result = $query->getResult();
		}
		return $result ? current($result) : NULL;
	}
	/**
	* Save results from array to DB
	* @param array results[login][part][column] = value
	* @param Entities\Teacher teacher who saves the results
	* @return TRUE or FALSE if parameters are wrong
	*/
	public static function saveResults($results,$teacher) {
		$em = Globals::getEntityManager();
		if(!is_array($results) || get_class($teacher) != 'Entities\Teacher') {
			return FALSE;
		}
		foreach($results as $login => $parts) {
			$student = $em->find('Entities\Student',$login);
			if($student) {
				foreach($parts as $partName => $columns) {
					foreach($columns as $column => $value) {
						$result = $student->results($partName,$column);
						/// if result doesn't exists yet and has some value -> create new object
						if(!$result && $value) {
							$result = new \Entities\Result;
							/// load part by name
							$part = $em->getRepository('Entities\Part')->findOneBy(array('name' => $partName));
							$result->student($student);
							$result->teacher($teacher);
							$result->part($part);
							$result->column($column);
							$result->value($value);
							$em->persist($result);
						/// else if value changes -> persist only
						} else if($result && $result->value() != $value) {
							$result->value($value);
							$em->persist($result);
						}
					}
				}
			}
		}
		$em->flush();
		return TRUE;
	}
}
?>