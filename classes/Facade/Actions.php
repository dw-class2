<?php
namespace Facade;
use Tools\Globals;

class Actions {
	/**
	* Get all actions
	* <code>$actions = Actions::getAllActions();</code>
	* @return array of Entities\Action or empty array
	*/
	public static function getAllActions() {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Action')->findAll();
	}
	/**
	* Get all singleshot actions (jednorazove akce)
	* <code>$actions = Actions::getAllSingleshotActions();</code>
	* @return array of Entities\Action or empty array
	*/
	public static function getAllSingleshotActions() {
		return Actions::getActionsByType(Globals::SINGLESHOT_ACTION_TYPE);
	}
	/**
	* Get all exams
	* <code>$actions = Actions::getAllExams();</code>
	* @return array of Entities\Action or empty array
	*/
	public static function getAllExams() {
		return Actions::getActionsByType(Globals::EXAM_ACTION_TYPE);
	}
	/**
	* Get action by id
	* <code>$action = Actions::getActionById(3);</code>
	* @param integer id
	* @return Entities\Action or NULL
	*/
	public static function getActionById($id) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Action')->find($id);
	}
	/**
	* Get action by original KOS id
	* <code>$action = Actions::getActionByOrigId('263993342705');</code>
	* @param string original KOS id
	* @return Entities\Action or NULL
	*/
	public static function getActionByOrigId($orig_id) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Action')->findOneBy(array('orig_id'=>$orig_id));
	}
	/**
	* Get actions by type
	* <code>$actions = Actions::getActionsByType('ZK');</code>
	* @param string action type
	* @return array of Entities\Action or empty array
	*/
	public static function getActionsByType($type) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Action')->findBy(array('type'=>$type));
	}
	/**
	* Get actions by student login
	* <code>$actions = Actions::getActionsByStudent('hanykkla');</code>
	* @param string student login
	* @return array of Entities\Action or empty array
	*/
	public static function getActionsByStudent($student_login) {
		return Actions::getActionsByTypeAndStudent(NULL,$student_login);
	}
	/**
	* Get actions by action type and student login
	* <code>$actions = Actions::getActionsByTypeAndStudent('JA','hanykkla');</code>
	* @param string action type
	* @param string student login
	* @return array of Entities\Action or empty array
	*/
	public static function getActionsByTypeAndStudent($type,$student_login) {
		$em = Globals::getEntityManager();
		$q = 'SELECT a FROM Entities\Action a JOIN a.students s WHERE s.login=:login'.($type ? ' AND a.type=:type' : '');
		$query = $em->createQuery($q);
		$query->setParameters(array('login'=>$student_login));
		if($type) {
			$query->setParameters(array('type'=>$type));
		}
		return $query->getResult();
	}
	/**
	* Get singleshot actions by student login
	* <code>$actions = Actions::getSingleshotActionsByStudent('hanykkla');</code>
	* @param string student login
	* @return array of Entities\Action or empty array
	*/
	public static function getSingleshotActionsByStudent($student_login) {
		return Actions::getActionsByTypeAndStudent(Globals::SINGLESHOT_ACTION_TYPE,$student_login);
	}
	/**
	* Get exams by student login
	* <code>$actions = Actions::getExamsByStudent('hanykkla');</code>
	* @param string student login
	* @return array of Entities\Action or empty array
	*/
	public static function getExamsByStudent($student_login) {
		return Actions::getActionsByTypeAndStudent(Globals::EXAM_ACTION_TYPE,$student_login);
	}
}