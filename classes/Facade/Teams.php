<?php
namespace Facade;
use Tools\Globals;

class Teams {
	/**
	* Get all teams
	* <code>$teams = Teams::getAllTeams();</code>
	* @return array of Entities\Team or empty array
	*/
	public static function getAllTeams() {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Team')->findAll();
	}
	/**
	* Get team by name
	* <code>$team = Teams::getTeamByName('Alpha');</code>
	* @param string team
	* @return Entities\Team or NULL
	*/
	public static function getTeamByName($team_name) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Team')->findOneBy(array('name'=>$team_name));
	}
	/**
	* Get teams by student (member) login
	* <code>$teams = Teams::getTeamsByStudent('zelenja8');</code>
	* @param string login
	* @return array of Entities\Team or empty array
	*/
	public static function getTeamsByStudent($student_login) {
		$em = Globals::getEntityManager();
		$q = 'SELECT t FROM Entities\Team t JOIN t.students s WHERE s.login=:login';
		$query = $em->createQuery($q);
		$query->setParameters(array('login'=>$student_login));
		return $query->getResult();
	}
}
?>