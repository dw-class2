<?php
namespace Facade;
use Tools\Globals,
	Exception\GenericException;

class Lessons {
	/**
	* Get lessons by course code and lesson type
	* <code>$lessons = Lessons::getLessonsByCourseAndType('BI-WMM','P');</code>
	* @param string course code
	* @param string lesson type
	* @return array of Entities\Lesson or empty array
	*/
	public static function getLessonsByCourseAndType($course_code,$lesson_type) {
		$em = Globals::getEntityManager();
		$cond = array();
		if($course_code) {
			$cond['course_code'] = $course_code;
		}
		if($lesson_type) {
			$cond['lesson_type'] = $lesson_type;
		}
		return $em->getRepository('Entities\Lesson')->findBy($cond);
	}
	/**
	* Get lessons by course code
	* <code>$lessons = Lessons::getLessonsByCourse('Y39TW1');</code>
	* @param string course code
	* @return array of Entities\Lesson or empty array
	*/
	public static function getLessonsByCourse($course_code) {
		return Lessons::getLessonsByCourseAndType($course_code,NULL);
	}
	/**
	* Get lessons by lesson type
	* <code>$lessons = Lessons::getLessonsByType('C');</code>
	* @param string lesson type
	* @return array of Entities\Lesson or empty array
	*/
	public static function getLessonsByType($lesson_type) {
		return Lessons::getLessonsByCourseAndType(NULL,$lesson_type);
	}
	/**
	* Get lesson by course code and lesson number
	* <code>$lesson = Lessons::getLessonByCourseAndNumber('Y39TW1','101');</code>
	* @param string course code
	* @param int lesson number
	* @return Entities\Lesson or NULL
	*/
	public static function getLessonByCourseAndNumber($course_code,$number) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Lesson')->findOneBy(array('course_code' => $course_code, 'number' => $number));
	}
	/**
	* Get lessons by courses code and lesson number
	* <code>$lessons = Lessons::getLessonsByCoursesAndNumber(array('Y39TW1','A4B39WA1'),'101');</code>
	* @param array of string course codes
	* @param int lesson number
	* @return array of Entities\Lesson or empty array
	*/
	public static function getLessonsByCoursesAndNumber($course_codes,$number) {
		$em = Globals::getEntityManager();
		if(!is_array($course_codes)) {
			throw new GenericException('getLessonsByCoursesAndNumber: argument is not an array');
		}
		if(empty($course_codes)) {
			return array();
		}
		$in = ''; $i = 1;
		foreach($course_codes as $code) {
			$in .= (($in) ? ',' : '')."?$i";
			$i++;
		}
		if($in != '') {
			$i = 1;
			$q = 'SELECT l FROM Entities\Lesson l WHERE l.course_code IN('.$in.') AND l.number = :number';
			$query = $em->createQuery($q);
			foreach($course_codes as $code) {
				$query->setParameter($i,$code);
				$i++;
			}
			$query->setParameters(array('number' => $number));
			return $query->getResult();
		}
		return array();
	}
}