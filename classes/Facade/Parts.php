<?php
namespace Facade;
use Tools\Globals,
	Tools\Debug,
	Facade\Courses;

class Parts {
	/**
	* Get all parts
	* <code>$parts = Parts::getAllParts();</code>
	* @return array of Entities\Part or empty array
	*/
	public static function getAllParts() {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Part')->findAll();
	}

	/**
	* Get parts by course code
	* <code>$parts = Parts::getPartsByCourse('BI-UOS');</code>
	* @return Doctrine\ORM\PersistentCollection ?? of Entities\Part or empty array
	*/
	public static function getPartsByCourse($course_code) {
		$course = Courses::getCourseByCode($course_code);
		if($course) {
			return $course->parts();
		} else {
			return array();
		}
	}

	/**
	* Get part by course code and part name
	* <code>$parts = Parts::getPartByCourseAndName('BI-UOS','tutorial');</code>
	* @return Entities\Part or NULL
	*/
	public static function getPartByCourseAndName($course_code,$part_name) {
		$em = Globals::getEntityManager();
		$q = 'SELECT p FROM Entities\Part p LEFT JOIN p.course c WHERE c.code=:course_code AND p.name=:part_name';
		$query = $em->createQuery($q);
		$query->setParameters(array('course_code' => $course_code));
		$query->setParameters(array('part_name' => $part_name));
		$r = $query->getResult();
		if(!empty($r)) {
			return current($r);
		} else {
			return NULL;
		}
	}
}
?>