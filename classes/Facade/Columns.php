<?php
namespace Facade;
use Tools\Globals,
	Tools\Debug,
	Facade\Parts;

class Columns {
	/**
	* Get columns by part
	* <code>$columns = Columns::getColumnsByCourseAndPart('BI-UOS','tutorial');</code>
	* @return array of Entities\Column or empty array
	*/
	public static function getColumnsByCourseAndPart($course_code,$part_name) {
$em = Globals::getEntityManager();
		$q = 'SELECT s FROM Entities\Column s LEFT JOIN s.part p LEFT JOIN p.course c WHERE c.code=:course_code AND p.name=:part_name ORDER BY s.idx';
		$query = $em->createQuery($q);
		$query->setParameters(array('course_code' => $course_code));
		$query->setParameters(array('part_name' => $part_name));
		return $query->getResult();
	}

	/**
	* Get column by part
	* <code>$columns = Columns::getColumnsByCoursePartAndVariable('BI-UOS','tutorial','z');</code>
	* @return Entities\Column or NULL
	*/
	public static function getColumnByCoursePartAndVariable($course_code,$part_name,$variable) {
		$em = Globals::getEntityManager();
		$q = 'SELECT s FROM Entities\Column s LEFT JOIN s.part p LEFT JOIN p.course c WHERE s.variable=:variable AND c.code=:course_code AND p.name=:part_name';
		$query = $em->createQuery($q);
		$query->setParameters(array('variable' => $variable));
		$query->setParameters(array('course_code' => $course_code));
		$query->setParameters(array('part_name' => $part_name));
		$r = $query->getResult();
		if(!empty($r)) {
			return current($r);
		} else {
			return NULL;
		}
	}

	/** Get Column by id
	* <code>$column = Columns::getColumnById($id);</code>
	* @return Entities\Column or NULL
	*/
	public static function getColumnById($id) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Column')->findOneBy(array('id'=>$id));
	}
}
?>