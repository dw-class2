<?php
namespace Facade;
use Tools\Globals,
	Exception\GenericException;

class Courses {
	/**
	* Get all courses (one course (or similar courses) per one DB intended)
	* e.g. BI-UOS + BIE-UOS or A4B39WA1 + A7B39WA1 + Y39TW1
	* <code>$courses = Courses::getAllCourses();</code>
	* @return array of Entities\Course or empty array
	*/
	public static function getAllCourses() {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Course')->findAll();
	}
	/**
	* Get course by code
	* <code>$course = Courses::getCourseByCode('Y39TW1');</code>
	* @param string course code
	* @return Entities\Course or NULL
	*/
	public static function getCourseByCode($code) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Course')->findOneBy(array('code' => $code));
	}
	/**
	* Get courses by codes
	* <code>$courses = Courses::getCoursesByCodes(array('Y39TW1','A7B39WA1'));</code>
	* @param array of string course code
	* @return array of Entities\Course or empty array
	*/
	public static function getCoursesByCodes($codes) {
		$em = Globals::getEntityManager();
		if(!is_array($codes)) {
			throw new GenericException('getCoursesByCodes: argument is not an array');
		}
		if(empty($codes)) {
			return array();
		}
		$in = ''; $i = 1;
		foreach($codes as $code) {
			$in .= (($in) ? ',' : '')."?$i";
			$i++;
		}
		if($in != '') {
			$i = 1;
			$q = 'SELECT c FROM Entities\Course c WHERE c.code IN('.$in.')';
			$query = $em->createQuery($q);
			foreach($codes as $code) {
				$query->setParameter($i,$code);
				$i++;
			}
		}
		return $query->getResult();
	}

	/**
	* Get course by language and study form (type)
	* <code>$courses = Courses::getCourseByLangType('cs','fulltime');</code>
	* @param string language (e.g. cs or en)
	* @param string type (e.g. fulltime or parttime)
	* @return Entities\Course or ??
	*/
	public static function getCourseByLangType($lang,$type) {
		$em = Globals::getEntityManager();
		return $em->getRepository('Entities\Course')->findOneBy(array('lang' => $lang, 'type' => $type));
	}

	/**
	* Get course by student who attends it
	* <code>$course = Courses::getCourseByStudent('alexaluk');</code>
	* @param string student login
	* @return Entities\Course or NULL
	*/
	/*
	public static function getCourseByStudent($student_login) {
		$em = Globals::getEntityManager();
		$student = $em->getRepository('Entities\Student')->findOneBy(array('login' => $student_login));
		if($student && (($lesson = $student->lecture()) || ($lesson = $student->laboratory()) || ($lesson = $student->tutorial()))) {
			return $lesson->course();
		} else {
			return NULL;
		}
	}*/
}
?>