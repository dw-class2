<?php
namespace Presenters;

use Tools\SyntaxHandler,
	UI\Table,
	Tools\DwHelper,
	Tools\Debug;

class HtmlTable implements IPresenter {
	public static function generateStudentList(SyntaxHandler $handler,$opts = array()) {
		global $ID;

		$students = $handler->getStudents();
		$parts = $handler->getParts();
		$table = new Table;
		if(isset($opts['editable'])) {
			$table->editable($opts['editable']);
			$table->form->csrf(DwHelper::getCSRF());
			$table->form->addHidden('id',$ID);
			$table->form->action("?id=$ID");
			$table->form->setSubmit('Save');
		}
		
		if(!empty($students)) {
			//--table header--
			$table->addHeadRow()->add('login')->add('name')->add('pno')->add('cno');
			foreach($parts as $part) {
				//get first course for column headers
				if($courses = $handler->getCourses()) {
					$course = current($courses);
				} else {
					$course = NULL;
				}
				$columns = $part->columns();
				foreach($columns as $column) {
					$table->add($column->variable());
				}
			}
			//--table body--
			foreach($students as $student) {
				$table->addBodyRow()
					->add($student->login())
					->add($student->name())
					->add($student->lecture() ? $student->lecture()->number() : "n/a")
					->add($student->tutorial() ? $student->tutorial()->number() : "n/a");
				try {
					foreach($parts as $part) {
						$columns = $part->columns();
						foreach($columns as $column) {
							$result = $student->results($part->name(),$column->variable());
							$value = $result ? $result->value() : '';
							$table->addInput($value,'results['.$student->login().']['.$part->name().']['.$column->variable().']');
						}
					}
				} catch(Exception $e) {
					Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
				}
			}
			return $table;
		} else {
			return '<p>No data in list</p>';
		}
	}
}
?>