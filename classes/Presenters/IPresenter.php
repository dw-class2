<?php
namespace Presenters;

use Tools\SyntaxHandler;

interface IPresenter {
	public static function generateStudentList(SyntaxHandler $handler,$opts = array());
}
?>