<?php
namespace Exception;

class BadClassException extends GenericException {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($message = 'BadClassException: Object has invalid/unexpected class', $code = 0) {
		parent::__construct($message, $code);
	}
}
