<?php
namespace Exception;

class NotSpecException extends GenericException {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($message = 'NotSpecExcption: required parameter not specified', $code = 0) {
		parent::__construct($message, $code);
	}
}
