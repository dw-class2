<?php
namespace Exception;

class AlreadyExistsException extends GenericException {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($message = 'AlreadyExistsException: item already exists', $code = 0) {
		parent::__construct($message, $code);
	}
}
