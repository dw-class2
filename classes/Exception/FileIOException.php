<?php
namespace Exception;

class FileIOException extends GenericException {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($message = 'FileIOException: File operation failed', $code = 0) {
		parent::__construct($message, $code);
	}
}
