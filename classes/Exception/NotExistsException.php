<?php
namespace Exception;

class NotExistsException extends GenericException {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($message = 'NotExistsException: required item not exists', $code = 0) {
		parent::__construct($message, $code);
	}
}
