<?php
namespace Exception;

class GenericException extends \Exception {
	/**
	 * Sets error message
	 * @param $code - id code - unused
	 * @param $messsage - exception message
	 */
	public function __construct($message = 'GenericException', $code = 0) {
		parent::__construct($message, $code);
	}
}
