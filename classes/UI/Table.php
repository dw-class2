<?php
namespace UI;

use UI\Form;

class Table {

	private $cssClass = NULL;
	private $data = array();
	private $rows = array('head' => 0, 'body' => 0);
	private $rowType = 'body';
	private $editable = TRUE;
	public $form = NULL;

	public function __construct() {
		$this->form = new Form;
	}

	//whether is table editable
	public function editable($editable = NULL) {
		if($editable !== NULL) {
			$this->editable = $editable;
		} else {
			return $this->editable;
		}
	}

	//css class of table element
	public function cssClass($cssClass = NULL) {
		if($cssClass !== NULL) {
			$this->cssClass = $cssClass;
		} else {
			return $this->cssClass;
		}
	}
	//css class of form element
	public function formCssClass($formCssClass = NULL) {
		if($cssClass !== NULL) {
			$this->formCssClass = $formCssClass;
		} else {
			return $this->formCssClass;
		}
	}


	protected function addRow() {
		$this->rows[$this->rowType]++;
		$this->data[$this->rowType][$this->rows[$this->rowType]] = array();
	}

	public function addHeadRow() {
		$this->rowType = 'head';
		$this->addRow();

		return $this;
	}

	public function addBodyRow() {
		$this->rowType = 'body';
		$this->addRow();

		return $this;
	}

	public function add($content = NULL, $name = NULL, $class = 'UI\TableCell', $cssClass = NULL) {
		if($class == 'UI\TableCell' && $this->rowType == 'head') {
			$class = 'UI\TableHeadCell';
		}
		$this->data[$this->rowType][$this->rows[$this->rowType]][] = new $class($content,$name,$cssClass);
		return $this;
	}

	// generic component adder
	public function __call($method, $args) {
		if(preg_match('/add([A-z][a-z]*)/',$method,&$match) && !empty($match[1])) {
			$class = 'UI\Table'.$match[1];
			if(@class_exists($class)) {
				$args[0] = isset($args[0]) ? $args[0] : NULL;
				$args[1] = isset($args[1]) ? $args[1] : NULL;
				$args[2] = isset($args[2]) ? $args[2] : NULL;
				return $this->add($args[0],$args[1],$class,$args[2]);
			}
		}
	}

	public function getData($bodyOnly = TRUE) {
		if($bodyOnly) {
			return $this->data['body'];
		} else {
			return $this->data;
		}
	}

	public function renderClassicTable() {
		$r = '';
		if($this->rows['head'] + $this->rows['body']) {
			$r .= "\n".'<div class="'.($this->editable ? 'editable' : 'readonly').'Table objTable">';
			if($this->editable && $this->form) {
				$r .= "\n".$this->form->printHeader();
			}
			$r .= "\n".'<table class="inline'.($this->cssClass ? ' '.$this->cssClass.'"' : '').'">';
			$rowTypes = array('head','body');
			foreach($rowTypes as $rowType) {
				if($this->rows[$rowType]) {
					$r .= "\n".'<t'.$rowType.'>';
					foreach($this->data[$rowType] as $row) {
						$r .= "\n".'<tr>';
						foreach($row as $column) {
							if(!$this->editable) {
								$r .= "\n".$column->nonEditable();
							} else {
								$r .= "\n".$column;
							}
						}
						$r .= "\n".'</tr>';
					}
					$r .= "\n".'</t'.$rowType.'>';
				}
			}
			$r .= "\n".'</table>';
			if($this->editable && $this->form) {
				$r .= "\n".$this->form->printFooter();
			}
			$r .= "\n".'</div>';
		}
		return $r;
	}

	public function __toString() {
		return $this->renderClassicTable();
	}

}