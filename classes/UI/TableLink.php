<?php
namespace UI;

use UI\TableCell;

class TableLink extends TableCell {
	public function nonEditable() {
		return __toString();
	}

	public function __toString() {
		if(is_array($this->content)) {
			$r = '<td>';
			foreach($this->content as $text => $link) {
				$r .= '<a href="'.$link.'" '.($this->cssClass ? ' class="'.$this->cssClass.'"' : '').'>'.
					$text.
					'</a>';
			}
			$r .= '</td>';
			return $r;
		} else {
			return '<td'.($this->cssClass ? ' class="'.$this->cssClass.'"' : '').'>(link)</td>';
		}
	}
}
?>