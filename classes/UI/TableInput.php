<?php
namespace UI;

use UI\TableCell;

class TableInput extends TableCell {
	public function nonEditable() {
		return parent::__toString();
	}

	public function __toString() {
		$id = strtr($this->name,array(
			'[' => '_',
			']' => '_',
			' ' => '_'
		));
		return '<td><input'.($this->cssClass ? ' class="'.$this->cssClass.'"' : '').' type="text" id="'.$id.'" name="'.$this->name.'" value="'.$this->content.'" /></td>';
	}
}
?>