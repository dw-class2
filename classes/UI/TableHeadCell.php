<?php
namespace UI;

use UI\TableCell;

class TableHeadCell extends TableCell {
	public function nonEditable() {
		return $this->__toString();
	}

	public function __toString() {
		return '<th'.($this->cssClass ? ' class="'.$this->cssClass.'"' : '').'>'.$this->content.'</th>';
	}
}
?>