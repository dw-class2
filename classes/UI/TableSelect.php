<?php
namespace UI;

use UI\TableCell;

class TableSelect extends TableCell {

	public function nonEditable() {
		$selected = isset($this->content['selected']) ? $this->content['selected'] : '(select)';
		return '<td'.($this->cssClass ? ' class="'.$this->cssClass.'"' : '').'>'.$selected.'</td>';
	}

	public function __toString() {
		/*
		* content[selected]
		* content[data] = array
		*/
		if(is_array($this->content) && !empty($this->content['data'])) {
			$select = '<td><select'.($this->cssClass ? ' class="'.$this->cssClass.'"' : '').' name="'.$this->name.'">';
			foreach($this->content['data'] as $key => $value) {
				$select .= "\n\t".'<option value="'.$key.'"'.
					(isset($this->content['selected'])
					&& ($key == $this->content['selected']) ? ' selected="selected"' : '').
					'>'.
					$value.
					'</option>';
			}
			$select .= "\n</select></td>";
			return $select;
		} else {
			return '<td'.($this->cssClass ? ' class="'.$this->cssClass.'"' : '').'>(select)</td>';
		}
	}
}
?>