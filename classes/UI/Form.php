<?php
namespace UI;

class Form {
	private $action;
	private $csrf;
	private $cssClass;
	private $submitValue = 'Submit';
	private $submitName = 'submitButton';
	private $hiddens = array();

	public function action($action = NULL) {
		if($action !== NULL) {
			$this->action = $action;
		} else {
			return $this->action;
		}
	}

	public function cssClass($cssClass = NULL) {
		if($action !== NULL) {
			$this->cssClass = $cssClass;
		} else {
			return $this->cssClass;
		}
	}

	//csrf hidden value
	public function csrf($csrf = NULL) {
		if($csrf !== NULL) {
			$this->csrf = $csrf;
		} else {
			return $this->csrf;
		}
	}

	public function addHidden($name, $value) {
		$this->hiddens[$name] = $value;
	}

	//submit button
	public function setSubmit($value, $name = 'submitButton') {
		$this->submitValue = $value;
		$this->submitName = $name;
	}

	public function printHeader() {
		$r = '<form'.
			($this->cssClass ? ' class="'.$this->cssClass.'"' : '').
			($this->action ? ' action="'.$this->action.'"' : '').
			' method="post">';
		if($this->csrf) {
			$r .= "\n".'<input type="hidden" name="sectok" value="'.$this->csrf.'" />';
		}
		foreach($this->hiddens as $name => $value) {
			$r .= "\n".'<input type="hidden" name="'.$name.'" value="'.$value.'" />';
		}
		return $r;
	}

	public function printFooter() {
		$r = "\n".'<input type="submit" name="'.$this->submitName.'" value="'.$this->submitValue.'" />'.
			"\n".'</form>';
		return $r;
	}
}
?>