<?php
namespace UI;

class TableCell {
	protected $name = NULL;
	protected $cssClass = NULL;
	protected $content = NULL;
	
	public function __construct($content = NULL, $name = NULL, $cssClass = NULL) {
		$this->content($content);
		$this->name($name);
		$this->cssClass($cssClass);
	}
	
	public function content($content = NULL) {
		if($content !== NULL) {
			$this->content = $content;
		} else {
			return $this->content;
		}
	}

	public function name($name = NULL) {
		if($name !== NULL) {
			$this->name = $name;
		} else {
			return $this->name;
		}
	}

	public function cssClass($cssClass = NULL) {
		if($cssClass !== NULL) {
			$this->cssClass = $cssClass;
		} else {
			return $this->cssClass;
		}
	}

	public function nonEditable() {
		return $this->__toString();
	}

	public function __toString() {
		return '<td'.($this->cssClass ? ' class="'.$this->cssClass.'"' : '').'>'.$this->content.'</td>';
	}
}
?>