<?php
namespace Tools;

use Exception\GenericException;

class Expression {
	const TOKEN_UNDEF = -1;	//undefined
	const TOKEN_SPACE = 0;	//whitespace
	const TOKEN_ARTOP = 1;	//arithmetic operator
	const TOKEN_CMPOP = 2;	//comparison operator
	const TOKEN_LOGOP = 3;	//logical operator
	const TOKEN_NUM = 4;	//number
	const TOKEN_PAREN = 5;	//parenthesis
	const TOKEN_IDENT = 6;	//identifier
	const TOKEN_STR = 7;	//string
	const TOKEN_FUNC = 8;	//function
	const TOKEN_FNAS = 9;	//function argument separator

	private static $token_groups = array(
		TOKEN_SPACE => " \t\r\n",
		TOKEN_ARTOP => '~+-*/^',
		TOKEN_CMPOP => '<>=!',
		TOKEN_LOGOP => '&|?:',
		TOKEN_NUM => '0123456789.,',
		TOKEN_PAREN => '()',
		TOKEN_IDENT => '$_ěščřžýáíéúůďťňóĚŠČŘŽÝÁÍÉÚŮĎŤŇÓ', //+a..z + A..Z
		TOKEN_STR => "`'\"",
		TOKEN_FUNC => NULL,
		TOKEN_FNAS => ';', //function argument separator
	);

	//operator's precedence (parenthesis are handled differently)
	private static $op_precedence = array(
		';' => -1,  //function argument separator -- lowest priority
		'(' => -1,
		')' => -1,

		'?' => -1,	//ternary operator - condition ? true : false
		':' => -1,	//ternary operator

		'&&' => 0,	//logical and
		'||' => 0,	//logical or

		'<' => 1,	//less
		'<=' => 1,	//less or equal
		'>' => 1,	//greater
		'>=' => 1,	//greater or equal

		'!=' => 2,	//not equal
		'==' => 2,	//equal

		'&' => 3,	//concatenating

		'+' => 3,	//add
		'-' => 3,	//subtract

		'*' => 4,	//multiple
		'/' => 4,	//divide

		'!' => 5,	//not

		'^' => 6,	//power

		'~' => 7,	//unary negation -- highest priority
	);

	private static $functions = array(
		"min", "max", "ceil", "floor", "round", "mark", "empty"
	);

	/**
	 * return in which group token belongs - number, operator, whitespace...
	 */
	private static function getTokenGroup($c) {

		if(($c >= 'a' && $c <= 'z') || ($c >= 'A' && $c <= 'Z')) {
			return TOKEN_IDENT;
		}
		foreach(self::$token_groups as $i => $tgroup) {
			if(strchr($tgroup,$c)) {
				return $i;
			}
		}
		return TOKEN_UNDEF;
	}

	/**
	 * convert string to token array
	 */
	public static function tokenize($s) {
		//init
		$len = mb_strlen($s);
		$i = 0; //position in string s
		$tok_count = 0;
		//tok_group0, tok0 - very first character
		//while characters available before end of string
		while($i < $len) {
			//get token group (number, operator...)
			$tok_group0 = self::getTokenGroup($s[$i]);
			$tok = $s[$i];
			$str_mode = ($tok_group0 == TOKEN_STR);
			$str_char = $s[$i];
			$i++;
			//while next character(s) are from same (or compatibile) token group and character is not parenthesis
			// or character is dot which doesn't split tokens
			// or number(s) follows letter
			while($i < $len && $tok_group0 != TOKEN_PAREN &&
						(($tok_group = self::getTokenGroup($s[$i])) == $tok_group0 ||
						$s[$i] == '.' ||
						($tok_group0 == TOKEN_IDENT && $tok_group == TOKEN_NUM) ||
						$str_mode
						)) {
				//concatenate characters to token
				$tok .= $s[$i];
				$str_mode = ($str_mode && $s[$i] != $str_char);
				$i++;
			}
			//if token isn't whitespace
			if($tok_group0 != TOKEN_SPACE) {
				//save token to token array
				$tok0 = $tok[0];
				if($tok_group0 == TOKEN_UNDEF && (($tok0 >= 'A' && $tok0 <= 'Z') || ($tok0 >= 'a' && $tok0 <= 'z'))) {
					$tok_group0 = TOKEN_IDENT;
				}
				if($tok_group0 == TOKEN_IDENT && in_array($tok, self::$functions)) {
					$tok_group0 = TOKEN_FUNC;
				}
				if($tok_group0 == TOKEN_STR) {
					$tok = substr($tok,1,-1);
				}
				if($tok_group0 == TOKEN_NUM) {
					$tok = strtr($tok,',','.');
				}
				$tokens[$tok_count]['data'] = $tok;
				$tokens[$tok_count]['type'] = $tok_group0;
				$tok_count++;
			}
		}
		return $tokens;
	}

	/**
	 * Dijkstra's shunting yard algorithm - convert infix to postfix (RPN)
	 */
	public static function infixToPostfix($expr) {
		$stack = array();
		$output = array();

		if(is_array($expr)) {
			$tokens = &$expr;
		} else {
			$tokens = self::tokenize($expr);
		}

		$tok_count = count($tokens);
		$top = -1;
		//for each token
		for($i = 0; $i < $tok_count; $i++) {
			$tok = $tokens[$i]['data'];
			$tok_group = $tokens[$i]['type'];
			if($tok_group == TOKEN_UNDEF) {
				throw new GenericException('Undefined token in expression');
			}
			//if token is number or identifier
			if($tok_group == TOKEN_NUM || $tok_group == TOKEN_IDENT || $tok_group == TOKEN_STR) {
				//write it to output
				$output[] = $tokens[$i];
				continue;
			}
			//if token is function
			if($tok_group === TOKEN_FUNC) {
				//push token to stack
				$stack[++$top] = $tokens[$i];
				continue;
			}
			//if token is function argument separator
			if($tok == ';') {
				//while operator on the stack is not left parenthesis
				while($top >= 0 && $stack[$top]['data'] != '(') {
					//pop operator stack to output
					$output[] = array_pop($stack);
					$top--;
				}
				//if stack runs out - there's unmatched left parenthesis
				if($top<0) {
					throw new GenericException('Unmatched parenthesis in expression');
				}
			}
			//if token is operator
			if($tok_group == TOKEN_ARTOP || $tok_group == TOKEN_CMPOP || $tok_group == TOKEN_LOGOP) {
				//while operator is not left parenthesis and it's precedence is less or equal to precedence of operator on the top of operator stach
				while($top >= 0 && $stack[$top]['data'] != '(' && self::$op_precedence[$tok] <= self::$op_precedence[$stack[$top]['data']]) {
					//pop operator stack to output
					$output[] = array_pop($stack);
					$top--;
				}
				//push token to operator stack
				$stack[++$top] = $tokens[$i];
				continue;
			}
			//if token is left parenthesis
			if($tok == '(') {
				//push token to operator stack
				$stack[++$top] = $tokens[$i];
				continue;
			}
			//if token is right parenthesis
			if($tok == ')') {
				//while operator on the stack is not left parenthesis
				while($top >= 0 && $stack[$top]['data'] != '(') {
					//pop operator stack to output
					$output[] = array_pop($stack);
					$top--;
				}
				//if stack runs out - there's unmatched left parenthesis
				if($top<0) {
					throw new GenericException('Unmatched parenthesis in expression');
				}
				//pop left parenthesis (not to output)
				array_pop($stack);
				$top--;
				//if token on stack is function
				if($stack[$top]['type'] == TOKEN_FUNC) {
					//pop stack to output
					$output[] = array_pop($stack);
					$top--;
				}
				continue;
			}
		}
		//while operator stack is not empty
		while($top >= 0) {
			//if operator is left parenthesis - it's unmatched
			if($stack[$top]['data'] == '(') {
				throw new GenericException('Unmatched parenthesis in expression');
			}
			//pop it to output
			$output[] = array_pop($stack);
			$top--;
		}
		return $output;
	}

	/**
	 * evaluate expression in postfix (RPN)
	 */
	public static function evaluate($expr,$varTable = array()) {
		if(is_array($expr)) {
			$tokens = &$expr;
		} else {
			$tokens = self::infixToPostfix($expr);
		}
		$stack = array();
		$tok_count = count($tokens);
		$top = -1;
		for($i = 0; $i < $tok_count; $i++) {
			$tok = $tokens[$i]['data'];
			$tok_group = $tokens[$i]['type'];
			if($tok_group == TOKEN_NUM || $tok_group == TOKEN_STR) {
				$stack[++$top] = $tok;
				continue;
			}
			if($tok_group == TOKEN_IDENT) {
				if(isset($varTable[$tok])) {
					$stack[++$top] = $varTable[$tok];
					Debug::debug('Expression: '.$tok.' resolved as '.$vartable[$tok]);
				}
				continue;
			}
			if($tok_group == TOKEN_FUNC) {
				//===FUNCTIONS===
				if($tok == 'min') {
					$x = array_pop($stack);
					$y = array_pop($stack);
					//saving --, ++ once
					$top--;
					$stack[$top] = $x < $y ? $x : $y;
					continue;
				}
				if($tok == 'max') {
					$x = array_pop($stack);
					$y = array_pop($stack);
					//saving --, ++ once
					$top--;
					$stack[$top] = $x > $y ? $x : $y;
					continue;
				}
				if($tok == 'round') {
					$x = array_pop($stack);
					$y = array_pop($stack);
					//saving --, ++ once
					$top--;
					$stack[$top] = round($y,$x);
					continue;
				}
				if($tok == 'floor') {
					$x = array_pop($stack);
					if(($dot = strpos($x,'.')) === false) {
						$r = $x;
					} else {
						if($x > 0) {
							$r = substr($x,0,$dot);
						} else {
							$r = substr($x,0,$dot)-1;
						}
					}
					$stack[$top] = $r;
					continue;
				}
				if($tok == 'ceil') {
					$x = array_pop($stack);
					if(($dot = strpos($x,'.')) === false) {
						$r = $x;
					} else {
						if($x > 0) {
							$r = substr($x,0,$dot)+1;
						} else {
							$r = substr($x,0,$dot);
						}
					}
					$stack[$top] = $r;
					continue;
				}
				if($tok == 'mark') {
					$x = array_pop($stack);
					$y = array_pop($stack);
					//saving --, ++ once
					$top--;
					if($x != 0) {
						$r = ($y / $x) * 100;
						$stack[$top] = ($r < 50 ? 'F' :
							($r < 60 ? 'E' :
								($r < 70 ? 'D' :
									($r < 80 ? 'C' :
										($r < 90 ? 'B' : 'A')
									)
								)
							)
						);
					} else {
						throw new GenericException('Division by zero in mark() function inside expression');
					}
					continue;
				}
				if($tok == 'empty') {
					$x = array_pop($stack);
					$stack[$top] = ($x === '');
				}
			}
			//===OPERATORS===
			if($tok == '&') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $x . $y;
				continue;
			}
			if($tok == '+') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $x + $y;
				continue;
			}
			if($tok == '~') {
				$x = array_pop($stack);
				$stack[$top] = -$x;
				continue;
			}
			if($tok == '-') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y - $x;
				continue;
			}
			if($tok == '*') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $x * $y;
				continue;
			}
			if($tok == '/') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				if($x != 0) {
					$stack[$top] = $y / $x;
				} else {
					throw new GenericException('Division by zero inside expression');
				}
				continue;
			}
			if($tok == '^') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = pow($y,$x);
				continue;
			}
			if($tok == '!') {
				$x = array_pop($stack);
				$stack[$top] = !$x;
				continue;
			}
			if($tok == '==') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y == $x;
				continue;
			}
			if($tok == '!=') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y != $x;
				continue;
			}
			if($tok == '<=') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y <= $x;
				continue;
			}
			if($tok == '>=') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y >= $x;
				continue;
			}
			if($tok == '<') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y < $x;
				continue;
			}
			if($tok == '>') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y > $x;
				continue;
			}
			if($tok == '&&') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y && $x;
				continue;
			}
			if($tok == '||') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $y || $x;
				continue;
			}
			//ternary operator ? :
			if($tok == ':') {
				$x = array_pop($stack);
				$y = array_pop($stack);
				$c = array_pop($stack);
				//saving --, ++ once
				$top--;
				$stack[$top] = $c ? $y : $x;
				continue;
			}
		}
		return $stack[$top];
	}
}
?>