<?php
namespace Tools;

class DwHelper {

	public static function getDwContent($showId = NULL) {
		global $conf;
		global $ID;

		if(!$showId) {
			$showId = $ID;
		}
		$filename = $conf['datadir'].'/'.str_replace(':','/',$showId.'.txt');
		if(is_readable($filename)) {
			return file_get_contents($filename);
		} else {
			return NULL;
		}
	}

	public static function getCSRF() {
		return getSecurityToken();
	}

	public static function checkCSRF($csrf) {
		return ($csrf === self::getCSRF());
	}

	public static function getSession($key,$default = NULL) {
		if(isset($_SESSION['class2'][$key])) {
			return $_SESSION['class2'][$key];
		} else {
			return $default;
		}
	}

	public static function setSession($key,$value) {
		session_name('DokuWiki');
		if($r = session_start()) {
			$_SESSION['class2'][$key] = $value;
		}
		return $r;
	}
}
?>