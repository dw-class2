<?php
namespace Tools;

class Debug {
	// debug target
	const DEBUG_OFF = 0;
	const DEBUG_ECHO = 1;
	const DEBUG_FILE = 2;
	const DEBUG_DW_MSG = 4;
	// debug message severity
	const DEBUG_ANC = 2;
	const DEBUG_OK = 1;
	const DEBUG_MSG = 0;
	const DEBUG_ERR = -1;

	private static $target = self::DEBUG_ECHO;

	public static function getDebugTarget() {
		return self::$target;
	}

	public static function setDebugTarget($target) {
		self::$target = $target;
	}
	
	public static function debug($data,$severity = self::DEBUG_MSG) {
		
		if(!self::$target) return;
		
		$text = print_r($data,true);
		if(self::$target & self::DEBUG_ECHO) {
			echo '<pre style="border: 1px solid black;">'.$text.'</pre>';
		}
		if(self::$target & self::DEBUG_DW_MSG) {
			msg($text,$severity);
		}
	}
	
	public static function datatype($var) {
		$r = '';
		$r .= is_bool($var) ? 'bool ' : '';
		$r .= is_numeric($var) ? 'numeric ' : '';
		$r .= is_int($var) ? 'int' : '';
		$r .= is_float($var) ? 'float' : '';
		$r .= is_string($var) ? 'string' : '';
		$r .= is_resource($var) ? 'resource' : '';

		if(is_array($var)) {
			$r .= (empty($var) ? 'empty ' : '').'array ';
		} else {
			$r .= is_null($var) ? 'NULL ' : '';
			$r .= is_object($var) ? get_class($var).' ' : '';
		}
		
		if(is_array($var)
			|| (is_object($var) && get_class($var)=='Doctrine\ORM\PersistentCollection')) {
			foreach($var as $first) break;
			if(!empty($first)) {
				$r .= 'of '.Debug::datatype($first);
			}
		}
		
		$r = trim($r);
		return $r;
	}
}
?>