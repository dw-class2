<?php
namespace Tools;

use Exception\NotSpecException;
use Exception\AlreadyExistsException;

class Globals {
	const LESSON_TYPE_LECTURE = 'P';
	const LESSON_TYPE_LABORATORY = 'L';
	const LESSON_TYPE_TUTORIAL = 'C';	
	
	const SINGLESHOT_ACTION_TYPE = 'JA';
	const EXAM_ACTION_TYPE = 'ZK';	
	
	private static $entityManager = NULL;
	
	public static function getEntityManager() {
		if(self::$entityManager) {
			return self::$entityManager;
		} else {
			throw new NotSpecException('Entity manager not specified');
		}
	}
	
	public static function setEntityManager($entityManager) {
		if(!self::$entityManager) {
			self::$entityManager = $entityManager;
		} else {
			throw new AlreadyExistsException('Entity manager is already specified');
		}
	}

}
?>