<?php

namespace Tools;

use Facade\Students,
	Facade\Courses,
	Facade\Parts,
	Tools\Debug,
	\Exception,
	Exception\GenericException,
	Doctrine\Common\Collections\ArrayCollection;

class SyntaxHandler {

	private $params = array();

	private $courses = array();
	private $parts = array();
	private $students = array();

	private function parseParams($str) {
		Debug::debug('parseParams: '.$str);
		$params = explode(':',trim($str," ~\n\t"));
		foreach($params as $param) {
			$var = explode('=',trim($param," \n\t"),2);
			if(isset($var[1])) {
				$vals = explode(',',trim($var[1]," \n\t"));
			} else {
				$vals = array();
			}
			$r[strtolower($var[0])] = $vals;
		}
		return($r);
	}

	public function isParam($param) {
		return array_key_exists($param,$this->params);
	}

	public function getParam($param,$single = false) {
		if($this->isParam($param)) {
			return $single ? current($this->params[$param]) : $this->params[$param];
		} else {
			return NULL;
		}
	}

	private function handleCourses() {
		try {
			if($codes = $this->getParam('courses')) {
				$lang = ''; $type = '';
				if($courses = Courses::getCoursesByCodes($codes)) {
					foreach($courses as $course) {
						if($lang == '' && $type == '') {
							$lang = $course->lang();
							$type = $course->type();
						} else {
							if($course->lang() != $lang || $course->type() != $type) {
								throw new GenericException('Courses have different langs or types');
							}
						}
						Debug::debug('handleCourses: '.$course->code());
					}
					if(!$lang || !$type) {
						throw new GenericException('Courses have undefined langs or types');
					}
					$this->courses = $courses;
				} else {
					throw new GenericException('No courses loaded by codes');
				}
			}
		} catch(Exception $e) {
			Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
		}
	}

	private function handleLangType() {
		try {
			if(($lang = $this->getParam('lang',true)) && ($type = $this->getParam('type',true))) {
				$this->courses = array(Courses::getCourseByLangType($lang,$type));
				Debug::debug('handleLangType: '.$lang.','.$type.' #courses: '.count($this->courses));
			}
		} catch(Exception $e) {
			Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
		}
	}

	private function handleLogins() {
		if($logins = $this->getParam('logins')) {
			try {
				$this->students = Students::getStudentsByLogins($logins);
				Debug::debug('handleLogins: items '.count($this->students));
			} catch(Exception $e) {
				Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
			}
		}
	}

	private function handleNum() {
		if($num = $this->getParam('num',true)) {
			try {
				$course_codes = array();
				foreach($this->courses as $course) {
					$course_codes[] = $course->code();
				}
				$this->students = Students::getStudentsByCoursesAndLesson($course_codes,$num);
				Debug::debug('handleNum: '.$num.' items: '.count($this->students));
			} catch(Exception $e) {
				Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
			}
		}
	}

	private function handleAction() {
		if($action = $this->getParam('action',true)) {
			try {
				$this->students = Students::getStudentsByActionOrigId($action);
				Debug::debug('handleAction: '.$action.' items: '.count($this->students));
			} catch(Exception $e) {
				Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
			}
		}
	}

	private function handleTeam() {
		if($team = $this->getParam('team',true)) {
			try {
				$this->students = Students::getStudentsByTeam($team);
				Debug::debug('handleTeam: '.$team.' items: '.count($this->students));
			} catch(Exception $e) {
				Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
			}
		}
	}

	private function handleParts() {
		$part_names = $this->getParam('parts');

		try {
			if($this->courses) {
				reset($this->courses);
				$course = current($this->courses);
			} else {
				throw new GenericException('No course for parts selection');
			}

			if($part_names && count($part_names)>0 && !empty($part_names[0])) {
				$this->parts = new ArrayCollection;
				foreach($part_names as $part_name) {
					if($part = Parts::getPartByCourseAndName($course->code(),$part_name)) {
						$this->parts->add($part);
					}
				}
				Debug::debug('handleParts: some #:'.count($this->parts));
			} else {
				$this->parts = Parts::getPartsByCourse($course->code());
				Debug::debug('handleParts: all #:'.count($this->parts));
			}
		} catch(Exception $e) {
			Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
		}
	}

	public function getParams() {
		return $this->params;
	}

	public function getCourses() {
		return $this->courses;
	}

	public function getParts() {
		return $this->parts;
	}

	public function getStudents() {
		return $this->students;
	}

	public function handle() {
		// get course or lang+type //
		try {
			if($this->isParam('courses')) {
				$this->handleCourses();
			} else if($this->isParam('lang') && $this->isParam('type')) {
				$this->handleLangType();
			} else {
				throw new GenericException('Unknown structure selector - need courses | lang+type');
			}
			// get students //
			if($this->isParam('logins')) {
				$this->handleLogins();
			} else if($this->isParam('num')) {
				$this->handleNum();
			} else if($this->isParam('action')) {
				$this->handleAction();
			} else if($this->isParam('team')) {
				$this->handleTeam();
			} else {
				throw new GenericException('Unknown student selector: need logins | num | action | team');
			}
		} catch(Exception $e) {
			Debug::debug($e->getMessage(),Debug::DEBUG_ERR);
		}
		// get parts //
		$this->handleParts();
	}
	
	public function __construct($match) {
		$this->params = $this->parseParams($match);
	}
}
?>