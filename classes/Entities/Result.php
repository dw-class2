<?php
namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;

/** @Entity @Table(name="results") */
class Result {
	/** @Id @Column(type="integer") @GeneratedValue */
	private $id;
	/* student_login - inaccessible, for length purposes*/
	/** @Column(name="student_login", length=10) */
	private $student_login;
	/**
	* @ManyToOne(targetEntity="Student", inversedBy="results")
	* @JoinColumn(name="student_login", referencedColumnName="login")
	*/
	private $student;
	/* teacher_login - inaccessible, for length purposes*/
	/** @Column(name="teacher_login", length=10) */
	private $teacher_login;
	/**
	* @ManyToOne(targetEntity="Teacher")
	* @JoinColumn(name="teacher_login", referencedColumnName="login")
	*/
	private $teacher;
	/**
	* @ManyToOne(targetEntity="Part")
	* @JoinColumn(name="part_id", referencedColumnName="id")
	*/
	private $part;
	/** @Column(name="`column`", type="string", length=45) */
	private $column;
	/** @Column(name="`value`", type="text", nullable=true) */
	private $value;
	/** @Column(type="text", nullable=true) */
	private $note;

	public function id() {
		return $this->id;
	}

	public function student(Student $student = NULL) {
		/* Result is "owning" side */
		if($student !== NULL) {
			$this->student = $student;
			$student->addResult($this);
		} else {
			return $this->student;
		}
	}

	public function teacher(Teacher $teacher = NULL) {
		if($teacher !== NULL) {
			$this->teacher = $teacher;
		} else {
			return $this->teacher;
		}
	}

	public function part(Part $part = NULL) {
		if($part !== NULL) {
			$this->part = $part;
		} else {
			return $this->part;
		}
	}

	public function column($column = NULL) {
		if($column !== NULL) {
			$this->column = $column;
		} else {
			return $this->column;
		}
	}

	public function value($value = NULL) {
		if($value !== NULL) {
			$this->value = $value;
		} else {
			return $this->value;
		}
	}

	public function note($note = NULL) {
		if($note !== NULL) {
			$this->note = $note;
		} else {
			return $this->note;
		}
	}
}