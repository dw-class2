<?php
namespace Entities;

/** @Entity @Table(name="actions") */
class Action {
	/** @Id @Column(type="integer") @GeneratedValue */
	private $id;
	/** @Column(type="bigint") */
	private $orig_id;
	/* course_code - inaccessible, for length purposes*/
	/** @Column(name="course_code", length=10) */
	private $course_code;
	/**
	* @ManyToOne(targetEntity="Course", inversedBy="actions")
	* @JoinColumn(name="course_code", referencedColumnName="code")
	*/
	private $course;
	/**
	* @ManyToOne(targetEntity="Teacher", inversedBy="actions")
	* @JoinColumn(name="teacher_login", referencedColumnName="login")
	*/
	private $teacher;
	/** @Column(type="string", length=5) */
	private $type;
	/** @Column(type="datetime") */
	private $date;
	/** @Column(type="string", length=20) */
	private $room;
	/** @Column(type="string", length=128) */
	private $name;
	/** @Column(type="text", nullable=true) */
	private $note;
	/**
	* @ManyToMany(targetEntity="Student", inversedBy="actions")
	* @JoinTable(name="action_has_student",
	*      joinColumns={@JoinColumn(name="action_id", referencedColumnName="id")},
	*      inverseJoinColumns={@JoinColumn(name="student_login", referencedColumnName="login")}
	*      )
	*/
	private $students = NULL;

	public function __construct() {
		$this->students = new ArrayCollection();
	}

	public function id() {
		return $this->id;
	}

	public function origId() {
		return $this->orig_id;
	}

	public function course(Course $course = NULL) {
		if($course !== NULL) {
			$this->course = $course;
		} else {
			return $this->course;
		}
	}

	public function teacher(Teacher $teacher = NULL) {
		if($teacher !== NULL) {
			$this->teacher = $teacher;
		} else {
			return $this->teacher;
		}
	}

	public function date($date = NULL) {
		if($date !== NULL) {
			$this->date = $date;
		} else {
			return $this->date;
		}
	}

	public function room($room = NULL) {
		if($room !== NULL) {
			$this->room = $room;
		} else {
			return $this->room;
		}
	}

	public function name($name = NULL) {
		if($name !== NULL) {
			$this->name = $name;
		} else {
			return $this->name;
		}
	}

	public function note($note = NULL) {
		if($note !== NULL) {
			$this->note = $note;
		} else {
			return $this->note;
		}
	}

	public function addStudent(Student $student) {
		/*Action is owning side*/
		$this->students()->add($student);
		$student->addAction($this);
	}

	public function students() {
		return $this->students;
	}
}