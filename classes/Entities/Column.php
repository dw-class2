<?php
namespace Entities;

use Tools\Debug,
	Exception\GenericException;

/** @Entity @Table(name="columns") */
class Column {
	const VALUE_VALID = 0;
	const VALUE_OUT_OF_RANGE = 1;
	const VALUE_NOT_MATCH = 2;
	const VALUE_NOT_IN_ENUM = 4;

	/** @Id @Column(type="integer") @GeneratedValue */
	private $id;
	/** @Column(type="integer") */
	private $idx;
	/** @Column(type="string", length=16) */
	private $variable;
	/** @Column(type="string", length=40) */
	private $caption;
	/** @Column(type="string", length=16) */
	private $type = 'string';
	/** @Column(type="string", length=100, nullable=true) */
	private $expression = NULL;
	/** @Column(type="string", length=100, nullable=true) */
	private $defaultValue = NULL;
	/** @Column(type="string", length=32, nullable=true) */
	private $validRange = NULL;
	/** @Column(type="string", length=100, nullable=true) */
	private $validRegex = NULL;
	/** @Column(type="string", length=100, nullable=true) */
	private $validValues = NULL;
	/**
	* @ManyToOne(targetEntity="Part", inversedBy="columns")
	* @JoinColumn(name="part_id", referencedColumnName="id")
	*/
	private $part = NULL;

	public function id($id = '@NULL') {
		if($id !== '@NULL') {
			$this->id = $id;
		} else {
			return $this->id;
		}
	}

	public function idx($idx = '@NULL') {
		if($idx !== '@NULL') {
			$this->idx = $idx;
		} else {
			return $this->idx;
		}
	}

	public function variable($var = '@NULL') {
		if($var !== '@NULL') {
			$this->variable = $var;
		} else {
			return $this->variable;
		}
	}

	public function caption($caption = '@NULL') {
		if($caption !== '@NULL') {
			$this->caption = $caption;
		} else {
			return $this->caption;
		}
	}

	public function type($type = '@NULL') {
		if($type !== '@NULL') {
			$this->type = $type;
		} else {
			return $this->type;
		}
	}

	public function expression($expression = '@NULL') {
		if($expression !== '@NULL') {
			$this->expression = $expression;
		} else {
			return $this->expression;
		}
	}

	public function defaultValue($defaultValue = '@NULL') {
		if($defaultValue !== '@NULL') {
			$this->defaultValue = $defaultValue;
		} else {
			return $this->defaultValue;
		}
	}

	public function validRange($validRange = '@NULL') {
		if($validRange !== '@NULL') {
			if(preg_match('/[0-9]*(.[0-9]*)?-[0-9]*(.[0-9]*)?/',$validRange) || $validRange === NULL) {
				$this->validRange = $validRange;
			} else {
				throw new GenericException("Invalid range definition: $validRange");
			}
		} else {
			return $this->validRange;
		}
	}

	public function validRegex($validRegex = '@NULL') {
		if($validRegex !== '@NULL') {
			$this->validRegex = $validRegex;
		} else {
			return $this->validRegex;
		}
	}

	public function validValues($validValues = '@NULL') {
		if($validValues !== '@NULL') {
			if(is_array($validValues)) {
				$this->validValues = implode(',',$validValues);
			} else {
				$this->validValues = $validValues;
			}
		} else {
			return explode(',',$this->validValues);
		}
	}

	public function isValid($value) {
		$r = 0;
		// check validRange
		list($validFrom,$validTo) = explode('-',$this->validRange);
		if(($validFrom != '' && $value < $validFrom) || ($validTo != '' && $value > $validTo)) {
			$r |= self::VALUE_OUT_OF_RANGE;
		}
		// check validRegex
		if($this->validRegex !== NULL && !preg_match($this->validRegex,$value)) {
			$r |= self::VALUE_NOT_MATCH;
		}
		// check validValues
		if(count($this->validValues) && !in_array($value,$this->validValues)) {
			$r |= self::VALUE_NOT_IN_ENUM;
		}
		return $r;
	}

	public function part(Part $part = NULL) {
		if($part !== NULL) {
			$this->part = $part;
			$part->addColumn($this);
		} else {
			return $this->part;
		}
	}


}