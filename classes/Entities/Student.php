<?php
namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;

/** @Entity @Table(name="students") */
class Student {
	/** @Id @Column(type="string", length=10) */
	private $login;
	/** @Column(type="integer") */
	private $number;
	/** @Column(type="string", length=100) */
	private $name;
	/*
	* when lecture/laboratory/tutorial = 0, then student is not registered to timetable
	*/
	/**
	* @ManyToOne(targetEntity="Lesson", inversedBy="p_students")
	* @JoinColumn(name="p_id", referencedColumnName="id")
	*/
	private $lecture;
	/**
	* @ManyToOne(targetEntity="Lesson", inversedBy="l_students")
	* @JoinColumn(name="l_id", referencedColumnName="id")
	*/
	private $laboratory;
	/**
	* @ManyToOne(targetEntity="Lesson", inversedBy="c_students")
	* @JoinColumn(name="c_id", referencedColumnName="id")
	*/
	private $tutorial;
	/** @Column(type="integer") */
	private $pno_kos;
	/** @Column(type="integer") */
	private $lno_kos;
	/** @Column(type="integer") */
	private $cno_kos;
	/** @Column(type="boolean") */
	private $deleted;
	/**
	* @OneToMany(targetEntity="Result", mappedBy="student")
	*/
	private $results = NULL;
	/**
	* @OneToMany(targetEntity="ResultHistory", mappedBy="student")
	*/
	private $results_history = NULL;
	/**
	* @ManyToMany(targetEntity="Action", mappedBy="students")
	*/
	private $actions = NULL;


	public function __construct() {
		$this->actions = new ArrayCollection();
		$this->teams = new ArrayCollection();
		$this->results = new ArrayCollection();
		$this->results_history = new ArrayCollection();
	}

	public function login($login = NULL) {
		if($login !== NULL) {
			$this->login = $login;
		} else {
			return $this->login;
		}
	}

	public function number($number = NULL) {
		if($number !== NULL) {
			$this->number = $number;
		} else {
			return $this->number;
		}
	}

	public function name($name = NULL) {
		if($name !== NULL) {
			$this->name = $name;
		} else {
			return $this->name;
		}
	}

	public function lecture(Lesson $lecture = NULL) {
		if($lecture !== NULL) {
			$this->lecture = $lecture;
			$lecture->addStudent($this);
		} else {
			return $this->lecture;
		}
	}

	public function laboratory(Lesson $laboratory = NULL) {
		if($laboratory !== NULL) {
			$this->laboratory = $laboratory;
			$laboratory->addStudent($this);
		} else {
			return $this->laboratory;
		}
	}

	public function tutorial(Lesson $tutorial = NULL) {
		if($tutorial !== NULL) {
			$this->tutorial = $tutorial;
			$tutorial->addStudent($this);
		} else {
			return $this->tutorial;
		}
	}

	public function deleted($deleted = NULL) {
		if($deleted !== NULL) {
			$this->deleted = $deleted;
		} else {
			return $this->deleted;
		}
	}

	public function addResult(Result $result) {
		/*Result is "owning" side - you should call Result->student! */
		$this->results()->add($result);
	}

	public function course() {
		//zjistit jaky predmet student navstevuje, zkousenim pres P/C/L...
		if(($lesson = $this->lecture) || ($lesson = $this->tutorial) || ($lesson = $this->laboratory)) {
			return $lesson->course();
		} else {
			return NULL;
		}
	}

	/*#### tohle je vsechno spatne, podle jmena part nemuzu identifikovat tu spravnou hodnotu
	co kdyz je jiny predmet, jazyk nebo typ, nez zamyslim??? ###
	neco z toho se nestane, pokud budu mit jen jeden predmet na DB, ale stejne...###*/
	public function results($part_name = NULL,$column = NULL) {
		if($part_name === NULL) {
			return $this->results;
		} else {
			if($column === NULL) {
				/*inline function uses $part from scope above*/
				return $this->results->filter(function($r) use($part_name) {
					return $r->part()->name() == $part_name;
				});
			} else {
				$r = $this->results->filter(function($r) use($part_name,$column) {
					return $r->part()->name() == $part_name && $r->column() == $column;
				});
				return count($r) ? $r->current() : NULL;
			}
		}
	}

	public function resultsHistory() {
		return $this->results_history;
	}

	public function addAction(Action $action) {
		/*Action is owning side - you should call Action->addStudent! */
		$this->actions()->add($action);
	}

	public function actions() {
		return $this->actions;
	}

	public function addTeam(Team $team) {
		/*Action is owning side - you should call Action->addStudent! */
		$this->teams()->add($team);
	}

	public function teams() {
		return $this->teams;
	}

}
?>
