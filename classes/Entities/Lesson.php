<?php
namespace Entities;
use Tools\Globals,
	Doctrine\Common\Collections\ArrayCollection;

/** @Entity @Table(name="lessons") */
class Lesson {
	/** @Id @Column(type="integer") @GeneratedValue */
	private $id;
	/* course_code - inaccessible, for length purposes*/
	/** @Column(name="course_code", length=10) */
	private $course_code;
	/**
	* @ManyToOne(targetEntity="Course", inversedBy="lessons")
	* @JoinColumn(name="course_code", referencedColumnName="code")
	*/
	private $course;
	/**
	* @ManyToMany(targetEntity="Teacher", inversedBy="lessons")
	* @JoinTable(name="lesson_has_teacher",
	*      joinColumns={@JoinColumn(name="lesson_id", referencedColumnName="id")},
	*      inverseJoinColumns={@JoinColumn(name="teacher_login", referencedColumnName="login")}
	*      )
	*/
	private $teachers = NULL;
	/** @Column(type="integer") */
	private $number;
	/** @Column(type="string", length=1) */
	private $lesson_type;
	/** @Column(type="integer") */
	private $week_day;
	/** @Column(type="string", length=1) */
	private $week_parity;
	/** @Column(type="integer") */
	private $time_from;
	/** @Column(type="integer") */
	private $time_to;
	/**
	* @OneToMany(targetEntity="Student", mappedBy="lecture")
	*/
	private $p_students = NULL;
	/**
	* @OneToMany(targetEntity="Student", mappedBy="laboratory")
	*/
	private $l_students = NULL;
	/**
	* @OneToMany(targetEntity="Student", mappedBy="tutorial")
	*/
	private $c_students = NULL;
	/* students - reference to [p|l|c]_students*/
	private $students = NULL;

	public function __construct() {
		$this->p_students = new ArrayCollection();
		$this->l_students = new ArrayCollection();
		$this->c_students = new ArrayCollection();
		$this->teachers = new ArrayCollection();
	}

	public function id() {
		return $this->id;
	}

	public function course(Course $course = NULL) {
		if($course !== NULL) {
			$this->course = $course;
			$course->addLesson($this);
		} else {
			return $this->course;
		}
	}

	public function addTeacher(Teacher $teacher) {
		/* Lesson is owning side */
		$this->teachers()->add($teacher);
		$teacher->addLesson($this);
	}

	public function teachers() {
			return $this->teachers;
	}

	public function number($number = NULL) {
		if($number !== NULL) {
			$this->number = $number;
		} else {
			return $this->number;
		}
	}

	public function lessonType($lesson_type = NULL) {
		if($lesson_type !== NULL) {
			$this->lesson_type = $lesson_type;
		} else {
			return $this->lesson_type;
		}
	}

	public function addStudent(Student $student) {
		$this->students()->add($student);
	}

	public function students() {
		if($this->students === NULL) {
			switch($this->lesson_type) {
				case Globals::LESSON_TYPE_LECTURE:
					$this->students = &$this->p_students;
					break;
				case Globals::LESSON_TYPE_LABORATORY:
					$this->students = &$this->l_students;
					break;
				case Globals::LESSON_TYPE_TUTORIAL:
					$this->students = &$this->c_students;
					break;
			}
		}
		return $this->students;
	}

}
?>