<?php
namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;

/** @Entity @Table(name="parts") */
class Part {
	/** @Id @Column(type="integer") @GeneratedValue */
	private $id;
	/** @Column(type="string", length=45) */
	private $name;
	/* course_code - inaccessible, for length purposes*/
	/** @Column(name="course_code", length=10) */
	private $course_code;
	/**
	* @ManyToOne(targetEntity="Course", inversedBy="parts")
	* @JoinColumn(name="course_code", referencedColumnName="code")
	*/
	private $course;
	/**
	* @ManyToMany(targetEntity="AuthGroup", inversedBy="parts")
	* @JoinTable(name="part_has_group",
	*      joinColumns={@JoinColumn(name="part_id", referencedColumnName="id")},
	*      inverseJoinColumns={@JoinColumn(name="group_id", referencedColumnName="id")}
	*      )
	*/
	private $groups;
	/**
	* @OneToMany(targetEntity="Column", mappedBy="part")
	*/
	private $columns = NULL;

	public function __construct() {
		$this->groups = new ArrayCollection();
		$this->columns = new ArrayCollection();
	}

	public function id() {
		return $this->id;
	}

	public function name($name = NULL) {
		if($name !== NULL) {
			$this->name = $name;
		} else {
			return $this->name;
		}
	}

	public function course(Course $course = NULL) {
		if($course !== NULL) {
			$this->course = $course;
			$course->addPart($this);
		} else {
			return $this->course;
		}
	}

	public function addGroup(AuthGroup $group) {
		/* Part is owning side */
		$this->groups()->add($group);
		$group->addPart($this);
	}


	public function groups() {
		return $this->groups;
	}

	public function addColumn(Column $column) {
		$this->columns()->add($column);
	}

	public function columns() {
		return $this->columns;
	}

}