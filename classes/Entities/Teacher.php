<?php
namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;

/** @Entity @Table(name="teachers") */
class Teacher {
	/** @Id @Column(type="string", length=10) */
	private $login;
	/** @Column(type="string", length=100) */
	private $name;
	/**
	* @ManyToMany(targetEntity="Lesson", mappedBy="teachers")
	*/
	private $lessons = NULL;
	/**
	* @ManyToMany(targetEntity="AuthGroup", inversedBy="teachers")
	* @JoinTable(name="teacher_has_group",
	*      joinColumns={@JoinColumn(name="teacher_login", referencedColumnName="login")},
	*      inverseJoinColumns={@JoinColumn(name="group_id", referencedColumnName="id")}
	*      )
	*/
	private $groups = NULL;

	public function __construct() {
		$this->lessons = new ArrayCollection();
		$this->groups = new ArrayCollection();
	}

	public function login($login = NULL) {
		if($login !== NULL) {
			$this->login = $login;
		} else {
			return $this->login;
		}
	}

	public function name($name = NULL) {
		if($name !== NULL) {
			$this->name = $name;
		} else {
			return $this->name;
		}
	}

	public function addLesson(Lesson $lesson) {
		/*Lesson is owning side - you should call Lesson->addTeacher! */
		$this->lessons()->add($lesson);
	}

	public function lessons() {
		return $this->lessons;
	}

	public function addGroup(AuthGroup $group) {
		/*Teacher is owning side*/
		$this->groups()->add($group);
		$group->addTeacher($this);
	}

	public function groups() {
		return $this->groups;
	}
}
?>