<?php
namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;

/** @Entity @Table(name="courses") */
class Course {
	/** @Id @Column(type="string", length=10) */
	private $code;
	/** @Column(type="string", length=60, nullable=true) */
	private $name;
	/** @Column(type="string", length=2) */
	private $lang;
	/** @Column(type="string", length=8) */
	private $type;
	/**
	* @OneToMany(targetEntity="Part", mappedBy="course")
	*/
	private $parts = NULL;
	/**
	* @OneToMany(targetEntity="Lesson", mappedBy="course")
	*/
	private $lessons = NULL;

	public function __construct() {
		$this->parts = new ArrayCollection();
		$this->lessons = new ArrayCollection();
	}

	public function code($code = NULL) {
		if($code !== NULL) {
			$this->code = $code;
		} else {
			return $this->code;
		}
	}

	public function name($name = NULL) {
		if($name !== NULL) {
			$this->name = $name;
		} else {
			return $this->name;
		}
	}

	public function lang($lang = NULL) {
		if($lang !== NULL) {
			$this->lang = $lang;
		} else {
			return $this->lang;
		}
	}

	public function type($type = NULL) {
		if($type !== NULL) {
			$this->type = $type;
		} else {
			return $this->type;
		}
	}

	public function addPart(Part $part) {
		$this->parts()->add($part);
	}

	public function parts() {
		return $this->parts;
	}

	public function addLesson(Lesson $lesson) {
		$this->lessons()->add($lesson);
	}

	public function lessons() {
		return $this->lessons;
	}
}
?>