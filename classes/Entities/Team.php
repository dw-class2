<?php
namespace Entities;

/** @Entity @Table(name="teams") */
class Team {
	/** @Id @Column(type="integer") @GeneratedValue */
	private $id;
	/** @Column(type="string", length=100) */
	private $name;
	/**
	* @ManyToMany(targetEntity="Student", inversedBy="teams")
	* @JoinTable(name="team_has_student",
	*      joinColumns={@JoinColumn(name="team_id", referencedColumnName="id")},
	*      inverseJoinColumns={@JoinColumn(name="student_login", referencedColumnName="login")}
	*      )
	*/
	private $students = NULL;

	public function __construct() {
		$this->students = new ArrayCollection();
	}

	public function id() {
		return $this->id;
	}

	public function name($name = NULL) {
		if($name !== NULL) {
			$this->name = $name;
		} else {
			return $this->name;
		}
	}

	public function addStudent(Student $student) {
		/*Action is owning side*/
		$this->students()->add($student);
		$student->addTeam($this);
	}

	public function students() {
		return $this->students;
	}
}