<?php
namespace Entities;
use Doctrine\Common\Collections\ArrayCollection;

/** @Entity @Table(name="auth_groups") */
class AuthGroup {
	/** @Id @Column(type="integer") @GeneratedValue */
	private $id;
	/** @Column(type="string", length=45) */
	private $name;
	/**
	* @ManyToMany(targetEntity="Part", mappedBy="groups")
	*/
	private $parts = NULL;
	/**
	* @ManyToMany(targetEntity="Teacher", mappedBy="groups")
	*/
	private $teachers = NULL;

	public function __construct() {
		$this->parts = new ArrayCollection();
		$this->groups = new ArrayCollection();
	}

	public function id() {
		return $this->id;
	}

	public function name($name = NULL) {
		if($name !== NULL) {
			$this->name = $name;
		} else {
			return $this->name;
		}
	}

	public function addPart(Part $part) {
		/*Part is owning side - you should call Part->addGroup! */
		$this->parts()->add($part);
	}

	public function parts() {
		return $this->parts;
	}

	public function addTeacher(Teacher $teacher) {
		/*Teacher is owning side - you should call Teacher->addGroup! */
		$this->teachers()->add($teacher);
	}

	public function teachers() {
		return $this->teachers;
	}

}