<?php
/**
 * Displays classification results
 */

use Tools\SyntaxHandler,
	Tools\Debug,
	Tools\DwHelper,
	Presenters\HtmlTable;

require_once(dirname(__FILE__).'/../bootstrap.php');

if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

/**
 * All DokuWiki plugins to extend the syntax function
 * need to inherit from this class
 */
class syntax_plugin_class2_main extends DokuWiki_Syntax_Plugin {

	public function __construct() {
		//Debug::setDebugTarget(Debug::DEBUG_OFF);
		Debug::setDebugTarget(Debug::DEBUG_DW_MSG);
		//Debug::setDebugTarget(Debug::DEBUG_ECHO);
	}

/**
 * overridden translation method
 * @return translation or code
 */
	public function getLang($code) {
		$translation = trim(parent::getLang($code));
		if(empty($translation))
			return $code;
		return $translation;
	}
/**
 * Returns basic information about the plug-in required by Dokuwiki
 * @return array with basic information
 */
	public function getInfo() {
		return array(
			'author'=> 'vojta jirkovsky',
			'email' => 'jirkovoj@fit.cvut.cz',
			'date' => '2010-2011',
			'name' => 'classification 2 - syntax',
			'desc' => 'classification 2 score syntax plugin (score viewing)',
			'url' => 'http://edux.fit.cvut.cz/',
		);
	}
/**
 * Plugin mode type is substitution
 * @return always string substitution
 */
	public function getType() {
		return 'substition';
	}
/**
 * Syntax substitues for a table - it is a block mode.
 * @return always string block
 */
	public function getPType() {
		return 'block';
	}
/**
 * Sort number
 * @return sort number
 */
	public function getSort() {
		return 999;
	}
/**
* Set entry pattern
* @param $mode - name of mode (dokuwiki internal)
*/
	public function connectTo($mode) {
		$this->Lexer->addSpecialPattern('^~~classification2:[^\n]*~~$',$mode,'plugin_class2_main');
	}

/**
 * Handle the match
 */
	public function handle($match, $state, $pos, &$handler) {
		global $ID;

		$syntaxH = new SyntaxHandler($match);
		$syntaxH->handle();
		$table = HtmlTable::generateStudentList($syntaxH,array('editable' => FALSE));
		return array(
			'misc' => date('j.n.Y H:i:s'),
			'data' => $table
			);
	}

/**
 * Create output
 */
	public function render($mode, &$renderer, $data) {
		global $ID;

		if($mode == 'xhtml') {
			$renderer->doc .= $data['misc'].'<br />'.$data['data'];
			return true;
		} else {
			return false;
		}
	}

}