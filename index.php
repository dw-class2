<?php
/// Classification 2 Playground ///
use Facade\Courses,
	Facade\Lessons,
	Facade\Students,
	Facade\Results,
	Facade\Teams,
	Facade\Actions,
	Tools\Debug;

require('bootstrap.php');

function microtime_float() {
    list($usec, $sec) = explode(' ', microtime());
    return ((float)$usec + (float)$sec);
}

function echo_datatype($var) {
	$r = '<div style="background: #b0ff81">';
	$r .= Debug::datatype($var);
	$r .= '</div>';
	echo $r;
}

$time_start = microtime_float();

/*
$col = new Entities\Column;
$col->validValues('1,2,3,4,7');
//$col->validValues(array('1','2,','3','4','7'));
$col->validRegex('/^[0-9]$/');
$col->validRange('-10');
Debug::debug($col->isValid(7));
Debug::debug($col->isValid(3));
Debug::debug($col->isValid(1));
Debug::debug($col->isValid('a'));
Debug::debug($col->isValid(0));
*/
//$students = Students::getStudentByLogin('alexaluk');
//$students = Students::getStudentsByLogins(array('adameji4','aletibha','arletjir'));
//$students = Students::getStudentsByCourseAndLesson('BI-UOS','101');
//$students = Students::getStudentsByLesson('109');
//$students = Students::getStudentsByCoursesAndLesson(array('A4B39WA1','A7B39WA1'),'102');
//$students = Students::getStudentsByCoursesAndLesson(array('BI-UOS'),'101');
$students = Students::getStudentsByCoursesAndLesson(array('BI-WMM'),'101');
//$students = Students::getStudentsByLessonTypeAndNumber('tutorial','102');
//$students = Students::getStudentsByLecture('1');
//$students = Students::getStudentsByTutorial('102');
//$students = Students::getStudentsByLaboratory('1102');
//$students = Students::getStudentsByTeam('Beta');
//$students = Students::getStudentsByActionOrigId('263993314205');
foreach($students as $student) {
	echo $student->name().':'.$student->login().':'.($student->tutorial() ? $student->tutorial()->course()->code() : "N/A").'<br />';
}
//echo_datatype($students);
//$courses = Courses::getAllCourses();
//$courses = Courses::getCourseByCode('Y39TW1');
//$courses = Courses::getCoursesByCodes(array('BI-UOS','BIE-UOS'));
$courses = Courses::getCoursesByCodes(array('BI-WMM'));
//$courses = Courses::getCourseByLangType('cs','fulltime');
//$courses = Courses::getCourseByStudent('adameji4');
echo_datatype($courses);
//$lessons = Lessons::getLessonsByCourse('BI-WMM');
//$lessons = Lessons::getLessonByCourseAndNumber('Y39TW1','101');
//$lessons = Lessons::getLessonsByCoursesAndNumber(array('Y39TW1','A4B39WA1'),'101');
//$lessons = Lessons::getLessonsByCourseAndType('BI-UOS','P');
//$lessons = Lessons::getLessonsByCourseAndType(NULL,'P');
//$lessons = Lessons::getLessonsByType('P');
//echo_datatype($lessons);
//$result = Results::getResult('alexaluk','tutorial','zapocet');
//echo_datatype($result);
//$teams = Teams::getAllTeams();
//$teams = Teams::getTeamByName('Alpha');
//$teams = Teams::getTeamsByStudent('zelenja9');
//echo_datatype($teams);
//$actions = Actions::getAllActions();
//$actions = Actions::getAllSingleshotActions();
//$actions = Actions::getAllExams();
//$actions = Actions::getActionsByType('JA');
//$actions = Actions::getActionsByStudent('hanykkla');
//$actions = Actions::getActionsByTypeAndStudent('JA','hanykkla');
//$actions = Actions::getSingleshotActionsByStudent('hanykkla');
//$actions = Actions::getExamsByStudent('hanykkla');
//echo_datatype($actions);

echo '<hr />';

echo '<h2>courses</h2>';

$courses = Courses::getAllCourses();
echo_datatype($courses);
foreach($courses as $course) {
	echo $course->code().' - '.$course->name().'<br />';
}

echo '<h2>students</h2>';

$students = Students::getStudentsByLecture(1);
echo_datatype($students);

//filtering!
$students = array_filter($students,function($i) { return substr($i->login(),0,1) == 'a'; });
echo_datatype($students);

foreach($students as $student) {
	echo $student->name().':'.$student->login().':'.($student->tutorial() ? $student->tutorial()->course()->code() : 'N/A').'<br />';
	foreach($student->results() as $result) {
		echo $result->part()->name().':'.$result->column().':'.$result->value().':'.$result->teacher()->name().'<br />';
	}
	foreach($student->results('tutorial') as $result) {
		echo $result->part()->name().':'.$result->column().':'.$result->value().':'.$result->teacher()->name().'<br />';
	}
	/*
	foreach($student->results('tutorial','a1') as $result) {
		echo $result->part()->name().':'.$result->column().':'.$result->value().'<br />';
	}
	*/
}

echo '<h2>students II</h2>';

$students = Students::getStudentsByLogins(array('adameji4','fialamat'));
echo_datatype($students);

//filtering!
$students = array_filter($students,function($i) { return substr($i->login(),0,1) == 'a'; });
echo_datatype($students);

foreach($students as $student) {
	echo $student->name().':'.$student->login().':'.($student->tutorial() ? $student->tutorial()->course()->code() : "N/A").'<br />';
	foreach($student->results() as $result) {
		echo $result->part()->name().':'.$result->column().':'.$result->value().':'.$result->teacher()->name().'<br />';
	}
	foreach($student->results('tutorial') as $result) {
		echo $result->part()->name().':'.$result->column().':'.$result->value().':'.$result->teacher()->name().'<br />';
	}
/*	foreach($student->results('tutorial','a1') as $result) {
		echo $result->part()->name().':'.$result->column().':'.$result->value().'<br />';
	}*/
}


$time_end = microtime_float();
$time = $time_end - $time_start;
echo "working time: $time<br />";

$time_start = microtime_float();

if($result = Results::getResult('alexaluk','tutorial','zapocet')) {
	echo_datatype($result);
	echo $result->value().'<br />';
} else {
	echo 'No result<br />';
}

$time_end = microtime_float();
$time = $time_end - $time_start;
echo "working time: $time<br />";

?>
